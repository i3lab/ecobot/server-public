# Ecobot server

## Prerequisites

This project requires NodeJS and NPM.
[Node](http://nodejs.org/) and [NPM](https://npmjs.org/) are really easy to install.
To make sure you have them available on your machine,
try running the following command.

```sh
npm -v && node -v
6.4.1
v8.16.0
```

## Installation
Clone the repo on your local machine:
```sh
git clone https://gitlab.com/i3lab/ecobot/server.git
cd server
```
To install and set up the library, run:

```sh
npm install
```

## Usage
### Running the app
```sh
npm start
```
