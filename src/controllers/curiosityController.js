const db = require("../models");
const Curiosity = db.curiosity;

/**
 * Create a new curiosity in the db, with related information
 */
exports.create = (req, res) => {
  const curiosity = {
    applianceName: req.body.applianceName,
    content: req.body.content,
    type: req.body.type,
  };

  Curiosity.create(curiosity)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Curiosity.",
      });
    });
};

/**
 * Load all curiosities for an appliance from the db
 */
exports.findAllForAppliance = (req, res) => {
  Curiosity.findAll({
    where: {
      applianceName: req.params.applianceName,
    },
  })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while retrieving curiosities for " +
            req.body.applianceName,
      });
    });
};

/**
 * Load all general curiosities
 */
exports.findAllGeneral = (req, res) => {
  Curiosity.findAll({
    where: {
      applianceName: null, // if appliance name is not specified, it means it is a general curiosity
    },
  })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while retrieving general curiosities",
      });
    });
};
