const readline = require("readline");
const ChatbotManager = require("./services/ChatbotManager");
const setUpServerAndDatabase = require("./utils/setUpServerAndDatabase");
const { setCurrentChatbotManager } = require("./utils/getCurrentManagers");

/**
 * Main file, where the server is set up and the chatbot is initialized.
 * It also contains an example interface to use the service without the API through the console
 * and the logic to make API work.
 */

const chatbotManager = new ChatbotManager(); // used to generate the answers to the questions
setCurrentChatbotManager(chatbotManager);

// set up the database and creates some mock data. Please note that during the dev phase the db is cleaned and reset
// to the default state (configured with the mockDatabaseData) after each restart. To disable this behaviour, see the setUpServerAndDatabase function.
setUpServerAndDatabase();

const rl = readline.createInterface({
  // used to read the user input from the console. Can be removed, as the answers can be done with API calls
  input: process.stdin,
  output: process.stdout,
});

const waitForUserInput = () => {
  // used to read the user input from the console. Can be removed, as the answers can be done with API calls
  rl.question("", function (input) {
    chatbotManager.answerToUser(input).then(() => waitForUserInput());
  });
};

waitForUserInput();
