/**
 * Area table is used to know the average consumptions for a given area
 */
module.exports = (sequelize, Sequelize) => {
  // noinspection JSUnresolvedVariable
  return sequelize.define("area", {
    areaName: {
      type: Sequelize.STRING,
      primaryKey: true, // in fact, there must be only one averageConsumptions per area
    },
    averageConsumptions: {
      type: Sequelize.INTEGER, // monthly average consumptions for that area (Source: ISTAT
    },
  });
};
