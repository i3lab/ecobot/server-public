/**
 * Consumption table is used to know the user consumptions for a given appliance,
 * and if there is a goal set for that appliance
 */
module.exports = (sequelize, Sequelize) => {
  // noinspection JSUnresolvedVariable
  return sequelize.define("consumption", {
    applianceName: {
      // the appliance the consumption row is referring to
      type: Sequelize.STRING,
      primaryKey: true, // in fact, there must be only one currentConsumption and at most one goal per appliance
    },
    currentConsumptions: {
      // the consumption for the current month of that appliance made by the user. Of course, it's a partial data and can be used just to compare it with the goal set (for example, at this speed, you will not reach the goal)
      type: Sequelize.INTEGER,
    },
    averageGlobalConsumptions: {
      // average GLOBAL consumption for that appliance in a month. This data is referring to the average consumption of all users for that appliance, and not just to the data of the current user. Can be used to set a default goal for a user that has never used that appliance (averageUserConsumptions = null)
      type: Sequelize.INTEGER,
    },
    averageUserConsumptions: {
      // average USER consumption for that appliance in a month. This data is referring to the average consumption of the user for that appliance in the past months. Can be used to compare the user with the area, and to set the default goal.
      type: Sequelize.INTEGER,
    },
    goal: {
      type: Sequelize.INTEGER,
    },
    goalPeriodEnd: {
      // when this goal period will end. When a new goal is set, the goal period will be current_date + 30 days. This data is useful to track the status of the goal (if the user is being able to reach the goal or not, based on currentConsumptions and goal)
      type: Sequelize.DATEONLY, // format: YYYY-MM-DD
    },
  });
};
