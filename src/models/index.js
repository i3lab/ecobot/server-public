const Sequelize = require("sequelize");
const { development } = require("../config/dbConfig");

// noinspection JSValidateTypes
const sequelize = new Sequelize(
  development.database,
  development.username,
  development.password,
  {
    host: development.host,
    dialect: development.dialect,
    logging: development.logging,
  }
);

// Add here a reference to all your models.
const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.user = require("./userModel.js")(sequelize, Sequelize);
db.area = require("./areaModel.js")(sequelize, Sequelize);
db.consumption = require("./consumptionModel.js")(sequelize, Sequelize);
db.suggestion = require("./suggestionModel.js")(sequelize, Sequelize);
db.curiosity = require("./curiosityModel.js")(sequelize, Sequelize);

module.exports = db;
