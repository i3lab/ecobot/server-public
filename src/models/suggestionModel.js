/**
 * Suggestion table is used to provide suggestions to the user for a given appliance
 */
module.exports = (sequelize, Sequelize) => {
  // noinspection JSUnresolvedVariable
  return sequelize.define("suggestion", {
    applianceName: {
      // the appliance the suggestion row is referring to. If null, the suggestion is general and not referring to a specific appliance
      type: Sequelize.STRING,
    },
    content: {
      // the suggestion the user will receive
      type: Sequelize.STRING,
    },
    type: {
      // can be useful to filter the suggestions for an appliance based on a parameter (eg: ECO_MODE suggestions, or TURN_OFF suggestions)
      type: Sequelize.STRING,
    },
  });
};
