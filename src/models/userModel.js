/**
 * User table is used to know the user data (e.g.: the region they are in, to compare with
 * average area consumptions)
 */
module.exports = (sequelize, Sequelize) => {
  // noinspection JSUnresolvedVariable
  return sequelize.define("user", {
    name: {
      type: Sequelize.STRING,
    },
    surname: {
      type: Sequelize.STRING,
    },
    region: {
      type: Sequelize.STRING,
    },
  });
};
