module.exports = (app) => {
  const answers = require("../controllers/answerController");

  let router = require("express").Router();

  // Get the answer
  router.get("/:message", answers.get);

  app.use("/api/answers", router);
};
