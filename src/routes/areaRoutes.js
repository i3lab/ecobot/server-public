module.exports = (app) => {
  const areas = require("../controllers/areaController.js");

  let router = require("express").Router();

  // Create a new Area object (will be used just to insert mock area values)
  router.post("/", areas.create);

  // Retrieve a single Area given the areaName
  router.get("/:areaName", areas.findOne);

  app.use("/api/areas", router);
};
