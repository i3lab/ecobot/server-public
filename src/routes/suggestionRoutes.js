module.exports = (app) => {
  const suggestions = require("../controllers/suggestionController");

  let router = require("express").Router();

  // Create a new Suggestion object
  router.post("/", suggestions.create);

  // Get all suggestions for an appliance
  router.get("/:applianceName", suggestions.findAllForAppliance);

  // Get all general suggestions
  router.get("/general/all", suggestions.findAllGeneral);

  app.use("/api/suggestions", router);
};
