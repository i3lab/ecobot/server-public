module.exports = (app) => {
  const users = require("../controllers/userController.js");

  let router = require("express").Router();

  // Create a new User
  router.post("/", users.create);

  // Retrieve a single User with id
  router.get("/:id", users.findOne);

  // Update the user with id
  router.put("/:id", users.update);

  app.use("/api/users", router);
};
