const strings = require("../data/strings");

/**
 * ContextManager is used in order to include available context in the user input.
 * It is used in order to provide the user with a more natural experience, without having to specify
 * every single detail in the input every time. For example, if the user says 'set a goal for TV' and,
 * immediately after, 'remove the goal', the ContextManager will automatically add the 'TV' context to the
 * second intent to make it 'remove the goal for TV'.
 *
 * In fact, ContextManager memorises in the context the entities recognised by nlp.js in the user inputs.
 *
 * Note: every context object have a 'tokens' property, which is used to remove the context after
 * a certain number of interaction if not used.
 *
 * Note 2: context is inserted in the input just if that entity is compulsory for the intent, and not already specified.
 * If the intent can work also without that entity, the context is not inserted.
 */
class ContextManager {
  constructor() {
    this._appliance = {
      // if not null, it can be used in order to include in the user intent the appliance they are referring to
      value: null,
      tokens: 0,
    };

    this._continuePreviousIntentTokens = 0; // if > 1, it means that there is a second part of an intent that is waiting user input (i.e. 'yes' answer) to be executed.
  }

  get appliance() {
    return this._appliance;
  }

  set appliance(value) {
    this._appliance = {
      value: value,
      tokens: 5, // when a new appliance object is set, tokens are set to five. This means that the context will be valid only for five intents.
    };
  }

  get continuePreviousIntentTokens() {
    return this._continuePreviousIntentTokens;
  }

  set continuePreviousIntentTokens(value) {
    // the function will be only valid for one intent. Then, we can assume the user has changed context.
    if (value <= 0) {
      this._continuePreviousIntentTokens = 0;
    } else {
      this._continuePreviousIntentTokens = value;
    }
  }

  // update the context with the entities recognised by nlp.js. This is the function that actually saves the information
  // in the context when recognised in the user input.
  addNewContext(entities) {
    entities.forEach((e) => {
      // if the entity is an appliance entity, save it in the appliance context
      if (e.entity === strings.applianceEntity) {
        this.appliance = e.option; // e.option is the value of the entity
      }
    });
  }

  // this is the function that includes the appliance context in the user input. So, 'set a goal' will become
  // 'set a goal for TV' if the appliance context is 'TV'
  includeAndUpdateApplianceContext(nlpAnswer, input) {
    let inputWithContext = input;
    if (this.appliance.value === null) {
      // if there is no appliance context, do nothing
      return inputWithContext;
    } else {
      this._appliance.tokens--; // ALWAYS decrease the number of tokens for the appliance context
      const conjunctionWord = {
        en: " for ",
        it: " per ",
      };
      if (nlpAnswer.slotFill?.currentSlot === strings.applianceEntity) {
        // include the context JUST if there is a compulsory slot for the appliance entity and the user did not specify it
        inputWithContext =
          input +
          conjunctionWord[strings.currentLanguage] +
          strings[
            this._appliance.value.toLowerCase().replace(/\s/g, "") + "Appliance"
          ][strings.currentLanguage][0]; // eg: if the recognised entity is Television, I'll look for strings.televisionAppliance, collecting the text in the currentLanguage // previous input + for + appliance context
      }
      if (this._appliance.tokens <= 0) {
        // tokens are finished. Reset the context
        this._appliance.value = null;
        this._appliance.tokens = 0;
      }
      return inputWithContext;
    }
  }

  // if updateContinuePreviousIntentSetContext was true, it should be reset to false since it's valid for just 1 intent
  updateContinuePreviousIntentTokensContext() {
    this.continuePreviousIntentTokens = this._continuePreviousIntentTokens - 1;
  }

  // the context will be automatically included in the user question if and only if:
  // 1) there is a context value for that variable (e.g. appliance)
  // 2) the context tokens are > 0
  // 3) the user question does not already contain the variable (e.g. appliance)
  // 4) there is a REQUIRED slot for that variable. So, if the variable is optional (eg: give me a suggestion for @appliance), the context will not be included
  //    > this can be inferred looking to the nlpAnswer.slotFill.currentSlot field. If it exists, it means that there
  //    is a compulsory slot that was not included in the user intent, and can be appended to the user question.
  async includeContext(input, manager) {
    let inputWithContext;

    // in order to discover if there is a missing slot in the user input, we need to process the user input
    // without the context. If this includes also 'nlpAnswer.slotFill?.currentSlot', it means that there is a missing slot.
    const preProcessingAnswer = await manager.process(
      strings.englishLanguage,
      input,
      null,
      null
    );

    // check if appliance context is needed
    inputWithContext = this.includeAndUpdateApplianceContext(
      preProcessingAnswer,
      input
    );

    this.updateContinuePreviousIntentTokensContext();

    // if new context data (entities) was recognised in the user input, update the context to include that data
    this.addNewContext(preProcessingAnswer.entities);

    if (strings.printLogs) {
      console.log("           LOG: context: " + JSON.stringify(this.appliance));
      console.log("           LOG: inputWithContext: " + inputWithContext);
    }

    return inputWithContext;
  }

  // this function is used to continue a previous intent after the user answers 'yes' to the YesNoIntent.
  // 1. The first part of the intent 'A' is run. That intent will save in the context (overriding this
  // continuePreviousIntent function) the second part of the intent that should be run (e.g. 'A2') optionally
  // (i.e.: if the user agrees, by answering 'yes' to the YesNoIntent).
  // 2. the chatbot enters the YesNoIntent, asking the user 'do you want to do action A2?'
  // 3. if the user answers 'yes', the YesNoIntent.intentAction will execute contextManager.continuePreviousIntent()
  // In this way, it's not necessary to know which was the previous intent and to execute it from scratch.
  // This function has a number of tokens = 1. This means that if the user changes context, it will be reset immediately.
  continuePreviousIntent() {}
}

module.exports = ContextManager;
