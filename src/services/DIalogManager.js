const strings = require("../data/strings");

/**
 * DialogManager is used in order to track the status of the dialog. For some intents, in fact,
 * is necessary to know the value of the previous intent, or other parameters, to know how to
 * continue the conversation. For example, after the user says 'set a goal', and the chatbot
 * asks them 'for which appliance', the chatbot needs to know that the previous intent was to
 * execute the action (in this case, set a goal for that appliance).
 */
class DialogManager {
  constructor() {
    this._currentIntent = null;
    this._previousIntent = null;
    this._specifyApplianceState = false;
  }

  get previousIntent() {
    return this._previousIntent; // get currentIntent is not necessary: it's useful to access just the lastIntent, since currentIntent can be always inferred
  }

  // this function is triggered when a new intent is executed. currentIntent becomes previousIntent, and the new intent becomes currentIntent
  set currentIntent(value) {
    this._previousIntent = this._currentIntent;
    this._currentIntent = value; // currentIntent is saved in order to set it as last when a new intent is run
  }

  // specifyApplianceState is necessary for the SpecifyApplianceIntent, in order to know if the user is trying
  // to specify an appliance name, or not. This is useful to avoid executing a wrong action if the user executes
  // SpecifyApplianceIntent out of the blue: in that case, a None intent is fired.
  get specifyApplianceState() {
    return this._specifyApplianceState;
  }

  // specifyApplianceState is set to TRUE if the user is executing an intent that requires to specify an appliance name,
  // without specifying it. In that case, the chatbot will ask the user to specify the appliance name through the slot
  // filling mechanism, and the user will enter the SpecifyApplianceIntent with specifyApplianceState = TRUE.
  set specifyApplianceState(nlpAnswer) {
    // nlp.js sets nlpAnswer.slotFill.currentSlot = applianceEntity if the intent that was executed has that slot and the user didn't specify it
    if (nlpAnswer.slotFill?.currentSlot === strings.applianceEntity) {
      this._specifyApplianceState = true;
    } else if (
      // otherwise, we are not in that state. An exception is the noneIntent and the specifyApplianceIntent itself,
      // that will not change the value of specifyApplianceState: so, if the user is executing the specifyApplianceIntent
      // but gives a wrong appliance name, the chatbot will ask the user to specify the appliance name again, and the
      // user will enter the SpecifyApplianceIntent with specifyApplianceState = TRUE, having another possibility to specify it correctly.
      nlpAnswer.intent !== strings.noneIntent &&
      nlpAnswer.intent !== strings.specifyApplianceIntent
    ) {
      this._specifyApplianceState = false;
    }

    if (strings.printLogs) {
      console.log(
        "           LOG: Appliance state: " + this._specifyApplianceState
      );
    }
  }
}

module.exports = DialogManager;
