const Intent = require("./Intent");
const strings = require("../../data/strings");

/**
 * This intent allows the user to exit the chatbot. It has no real utility, since
 * the chatbot can stay active anyway, but it's useful to correctly answer to the
 * user if needed.
 */

const utterances = {
  en: [
    "again",
    "another",
    "another one",
    "another one please",
    "next one",
    "next",
  ],
  it: ["un altro", "un altro per favore", "ancora uno", "prossimo", "di nuovo"],
};

class AgainIntent extends Intent {
  constructor(nlpManager) {
    super(
      strings.againIntent,
      utterances[strings.currentLanguage],
      [],
      nlpManager
    );
  }

  async intentAction(
    nlpAnswer,
    dialogManager,
    contextManager,
    intents,
    chatbotManager
  ) {
    const previousIntent = dialogManager.previousIntent;

    // if the previous intent was a suggestion or a curiosity, we can fetch another suggestion or curiosity for that intent
    // the context will fill the intent with the correct appliance
    if (
      previousIntent === strings.suggestionIntent ||
      previousIntent === strings.curiosityIntent
    ) {
      const utterance = intents.find((i) => i.intentName === previousIntent)
        ?.utterances[0]; // get the first utterance of the previous intent
      const applianceWithAt = "@" + strings.applianceEntity; // @appliance

      await chatbotManager
        .answerToUser(
          utterance.replace(applianceWithAt, ""), // remove @appliance: the context will update the intent with the correct appliance
          true
        )
        .then(() => {});
    } else {
      await chatbotManager.answerToUser("", false).then(() => {}); // fires a none intent. In fact, there is nothing to repeat
    }
  }
}

module.exports = AgainIntent;
