const Intent = require("./Intent");
const strings = require("../../data/strings");
const { loadConsumptionData } = require("../../utils/makeAPICall");

/**
 * This intent can be used by the user to convert the energy into monetary cost.
 * A fixed factor will be used: 0,551 €/kWh (in Italy, as of 1 december 2022).
 * Source: https://www.prontobolletta.it/costo-energia-elettrica/kwh/#:~:text=0%2C53282%20%E2%82%AC%2FkWh*,e%20trasporto%20stabiliti%20dai%20fornitori
 */

const utterances = {
  en: [
    "how much @appliance costs?",
    "what is the cost of @appliance?",
    "how much do i spend for @appliance?",
    "how much i'm spending for @appliance?",
  ],
  it: [
    "quanto costa @appliance?",
    "quanto costa l'@appliance?",
    "qual è il costo di @appliance?",
    "quanto spendo per @appliance?",
    "quanto spendo per @appliance ogni mese?",
    "quanto sto spendendo per @appliance?",
    "quanto ho speso per @appliance?",
    "spesa per @appliance questo mese",
    "qual è la mia spesa per @appliance di solito",
    "qual è stata la mia spesa per @appliance nell'ultimo periodo",
    "quali sono i costi di @appliance",
    "quanti euro spendo per @appliance",
    "costo di @appliance",
    "e il suo costo?",
  ],
};

class CostIntent extends Intent {
  constructor(nlpManager) {
    super(
      strings.costIntent,
      utterances[strings.currentLanguage],
      [],
      nlpManager
    );
  }

  async intentAction(
    nlpAnswer,
    dialogManager,
    contextManager,
    intents,
    chatbotManager
  ) {
    const energyCost = 0.551; // conversion factor, 0,551 €/kWh.

    // extract from the nlpAnswer the appliance name
    let currentAppliance;
    let currentApplianceText;
    for (const entity of nlpAnswer.entities) {
      if (entity.entity === strings.applianceEntity) {
        currentAppliance = entity.option;
        currentApplianceText = entity.sourceText;
      }
    }

    // if the appliance name was specified, we can continue. Otherwise, the slotFilling will ask the user to specify the appliance name, and we can close the intentAction
    if (!currentAppliance) {
      return;
    }

    // we can assume that the db is already filled with the list of all appliance in the appliances table
    const previousConsumptionsData = await loadConsumptionData(
      currentAppliance
    );

    // check if there are current (or previous) consumptions for that appliance
    let finalAnswer = "";
    if (previousConsumptionsData.data.currentConsumptions) {
      // during this month the user has already used that appliance. We can calculate the cost for the current month
      if (strings.currentLanguage === "en") {
        finalAnswer +=
          "This month, you have already spent " +
          Math.round(
            previousConsumptionsData.data.currentConsumptions * energyCost
          ) +
          "€ for " +
          currentApplianceText +
          ".";
      } else if (strings.currentLanguage === "it") {
        finalAnswer +=
          "Questo mese, hai già speso " +
          Math.round(
            previousConsumptionsData.data.currentConsumptions * energyCost
          ) +
          "€ per " +
          currentApplianceText +
          ".";
      }
    }

    if (previousConsumptionsData.data.averageUserConsumptions) {
      // we have average data for the user consumptions for the past months. We can calculate the cost for the average consumptions
      if (finalAnswer !== "") {
        finalAnswer += " "; // to put a space between the two sentences
      }
      if (strings.currentLanguage === "en") {
        finalAnswer +=
          "On average, during the last period, you have spent " +
          Math.round(
            previousConsumptionsData.data.averageUserConsumptions * energyCost
          ) +
          "€ for " +
          currentApplianceText +
          ".";
      } else if (strings.currentLanguage === "it") {
        finalAnswer +=
          "In media, invece, spendi " +
          Math.round(
            previousConsumptionsData.data.averageUserConsumptions * energyCost
          ) +
          "€ al mese per " +
          currentApplianceText +
          ".";
      }
    }

    if (finalAnswer === "") {
      // we don't have any data for the user.
      if (strings.currentLanguage === "en") {
        finalAnswer =
          "During this month and in the past, you haven't used " +
          currentAppliance +
          ".";
      } else if (strings.currentLanguage === "it") {
        finalAnswer =
          "Questo mese e in passato, non hai usato " +
          currentApplianceText +
          ".";
      }
    }

    chatbotManager.printChatbotAnswer(finalAnswer);
  }
}

module.exports = CostIntent;
