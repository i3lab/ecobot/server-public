const Intent = require("./Intent");
const strings = require("../../data/strings");
const {
  loadCuriositiesForAppliance,
  loadCuriositiesGeneral,
} = require("../../utils/makeAPICall");

/**
 * This intent allows the user to request a general curiosity or a curiosity
 * for a specific appliance.
 */

const utterances = {
  en: [
    "give me a curiosity for @appliance",
    "curiosity for @appliance",
    "please provide me a curiosity for @appliance",
  ],
  it: [
    "dammi una curiosità per @appliance",
    "dammi una curiosita per @appliance",
    "curiosità per @appliance",
    "curiosita per @appliance",
    "per favore dammi una curiosità per @appliance",
    "per favore dammi una curiosita per @appliance",
    "dammi un altra curiosita per @appliance",
    "dammi un altra curiosità per @appliance",
    "curiosità @appliance",
    "curiosita @appliance",
  ],
};

class CuriosityIntent extends Intent {
  constructor(nlpManager) {
    super(
      strings.curiosityIntent,
      utterances[strings.currentLanguage],
      [],
      nlpManager
    );
  }

  printCuriosity(curiositiesList, chatbotManager, applianceName) {
    if (curiositiesList.data.length > 0) {
      // if we found curiosities, we will send one of them, randomly
      const randomCuriosity =
        curiositiesList.data[
          Math.floor(Math.random() * curiositiesList.data.length)
        ].content;
      const introText = {
        en: "Here's a curiosity for you: ",
        it: "Ecco una curiosità per te: ",
      };
      chatbotManager.printChatbotAnswer(
        introText[strings.currentLanguage] + randomCuriosity
      );
    } else {
      // no curiosities are available for that appliance.
      if (applianceName) {
        let generatedAnswer;
        if (strings.currentLanguage === "en") {
          generatedAnswer =
            "I'm sorry. I'm still learning, and currently I don't know any curiosity for " +
            applianceName +
            ". Try asking me a general curiosity (by asking 'give me a suggestion for my house), or a curiosity for a different appliance.";
        } else if (strings.currentLanguage === "it") {
          generatedAnswer =
            "Mi dispiace. Sto ancora imparando, e al momento non conosco nessuna curiosità per " +
            applianceName +
            ". Prova a chiedermi una curiosità generale (chiedendo 'dammi un suggerimento per la mia casa'), o una curiosità per un elettrodomestico diverso.";
        }
        chatbotManager.printChatbotAnswer(generatedAnswer);
      } else {
        let generatedAnswer;
        if (strings.currentLanguage === "en") {
          generatedAnswer =
            "I'm sorry. I'm still learning, and currently I don't know any general curiosity. Try asking me for a curiosity for a specific appliance.";
        } else if (strings.currentLanguage === "it") {
          generatedAnswer =
            "Mi dispiace. Sto ancora imparando, e al momento non conosco nessuna curiosità generale. Prova a chiedermi una curiosità per un elettrodomestico.";
        }
        chatbotManager.printChatbotAnswer(generatedAnswer);
      }
    }
  }

  async intentAction(
    nlpAnswer,
    dialogManager,
    contextManager,
    intents,
    chatbotManager
  ) {
    let applianceName;
    let applianceNameText;
    for (const entity of nlpAnswer.entities) {
      if (entity.entity === strings.applianceEntity) {
        applianceName = entity.option;
        applianceNameText = entity.sourceText;
      }
    }

    // if the appliance name was specified, we can continue. Otherwise, the slotFilling will ask the user to specify the appliance name, and we can close the intentAction
    if (!applianceName) {
      return;
    }

    // if the user wants a suggestion for the entire house, we will load the general suggestions: a general suggestion does not have an appliance associated, so it's sufficient to set applianceName and applianceNameText to null
    if (applianceName === strings.entirehouseAppliance.id) {
      applianceName = null;
      applianceNameText = null;
    }

    if (applianceName) {
      // if the user specified an appliance, we will search for a curiosity for that appliance
      const curiosities = await loadCuriositiesForAppliance(applianceName);
      this.printCuriosity(curiosities, chatbotManager, applianceNameText);
    } else {
      const curiosities = await loadCuriositiesGeneral();
      this.printCuriosity(curiosities, chatbotManager, null);
    }
  }
}

module.exports = CuriosityIntent;
