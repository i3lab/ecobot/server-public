const Intent = require("./Intent");
const strings = require("../../data/strings");
const {
  loadConsumptionData,
  updateConsumption,
} = require("../../utils/makeAPICall");
const ContextManager = require("../ContextManager");

/**
 * This intent allows the user to decrease the difficulty of the goal of a specific appliance.
 * It is usually triggered after the EditGoalIntent.
 */

const utterances = {
  en: [
    "decrease goal for @appliance",
    "decrease for @appliance",
    "decrease @appliance goal",
    "goal for @appliance is too difficult",
    "difficult for @appliance",
    "too difficult for @appliance",
    "too complex for @appliance",
    "goal for @appliance is too complex",
  ],
  it: [
    "diminuisci la difficoltà dell'obiettivo per @appliance",
    "diminuisci la difficoltà del goal per @appliance",
    "diminuisci @appliance",
    "diminuiscine la difficotà @appliance",
    "difficile per @appliance",
    "il goal per @appliance è troppo difficile",
    "troppo difficile per @appliance",
    "troppo complesso per @appliance",
    "il goal per @appliance è troppo complesso",
    "il goal per @appliance è troppo difficile",
    "l'obiettivo per @appliance è troppo complesso",
    "l'obiettivo per @appliance è troppo difficile",
    "non riesco a raggiungere il goal per @appliance",
    "riduci @appliance",
    "riduci la difficoltà dell'obiettivo per @appliance",
    "l'obiettivo per @appliance è troppo difficile da raggiungere",
  ],
};

class DecreaseGoalIntent extends Intent {
  constructor(nlpManager) {
    super(
      strings.decreaseGoalIntent,
      utterances[strings.currentLanguage],
      [],
      nlpManager
    );
  }

  async intentAction(
    nlpAnswer,
    dialogManager,
    contextManager,
    intents,
    chatbotManager
  ) {
    // extract from the nlpAnswer the appliance name
    let currentAppliance;
    let currentApplianceText;
    for (const entity of nlpAnswer.entities) {
      if (entity.entity === strings.applianceEntity) {
        currentAppliance = entity.option;
        currentApplianceText = entity.sourceText;
      }
    }

    // if the appliance name was specified, we can continue. Otherwise, the slotFilling will ask the user to specify the appliance name, and we can close the intentAction
    if (!currentAppliance) {
      return;
    }

    // we can assume that the db is already filled with the list of all appliance in the appliances table
    const previousConsumptionsData = await loadConsumptionData(
      currentAppliance
    );

    // check if a goal is already set for the appliance
    if (!previousConsumptionsData.data.goal) {
      let generatedAnswer;
      if (strings.currentLanguage === "en") {
        generatedAnswer =
          "You still don't have a goal set for " +
          currentApplianceText +
          ". Do you want to set a goal for it now? ";
      } else if (strings.currentLanguage === "it") {
        generatedAnswer =
          "Non hai ancora impostato un obiettivo per " +
          currentApplianceText +
          ". Vuoi impostarlo adesso? ";
      }
      // for that appliance, there is no goal set.
      chatbotManager.printChatbotAnswer(generatedAnswer);

      // put in the context the set goal intent, in order to set a goal if the users answers yes in the yesNoIntent
      ContextManager.prototype.continuePreviousIntent = async function () {
        let generatedAnswer;
        if (strings.currentLanguage === "en") {
          generatedAnswer = "set a goal for " + currentApplianceText;
        } else if (strings.currentLanguage === "it") {
          generatedAnswer = generatedAnswer =
            "imposta un goal per " + currentApplianceText;
        }
        await chatbotManager
          .answerToUser(generatedAnswer, false)
          .then(() => {});
      };

      // set the continuePreviousIntent action as valid (for 1 intent)
      contextManager.continuePreviousIntentTokens = 2;
    } else {
      await updateConsumption(
        currentAppliance,
        previousConsumptionsData.data.currentConsumptions,
        previousConsumptionsData.data.averageGlobalConsumptions,
        previousConsumptionsData.data.averageUserConsumptions,
        Math.round(previousConsumptionsData.data.goal * 1.1), // the goal is 10% easier now
        previousConsumptionsData.data.goalPeriodEnd
      );
      let generatedAnswer;
      if (strings.currentLanguage === "en") {
        generatedAnswer =
          "Okay! I'll decrease by 10% the difficulty of the goal. Now, you have to save " +
          Math.round(previousConsumptionsData.data.goal * 1.1) +
          " kWh instead of " +
          previousConsumptionsData.data.goal +
          " for " +
          currentApplianceText +
          "!";
      } else if (strings.currentLanguage === "it") {
        generatedAnswer =
          "Ok! Diminuisco del 10% la difficoltà dell'obiettivo. Ora, dovrai risparmiare solo " +
          Math.round(previousConsumptionsData.data.goal * 1.1) +
          " kWh invece di " +
          previousConsumptionsData.data.goal +
          " per " +
          currentApplianceText +
          "!";
      }
      chatbotManager.printChatbotAnswer(generatedAnswer);
    }
  }
}

module.exports = DecreaseGoalIntent;
