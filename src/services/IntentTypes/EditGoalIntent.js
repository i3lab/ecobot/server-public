const Intent = require("./Intent");
const strings = require("../../data/strings");
const { loadConsumptionData } = require("../../utils/makeAPICall");
const ContextManager = require("../ContextManager");

/**
 * This intent allows the user to edit the complexity goal of a specific appliance.
 * The idea is that the user will answer to the chatbot with 'too difficult' or
 * 'too easy', triggering the DecreaseGoalIntent or IncreaseGoalIntent.
 */

const utterances = {
  en: [
    "edit a goal for @appliance",
    "edit the goal for @appliance",
    "change a goal for @appliance",
    "change the goal for @appliance",
  ],
  it: [
    "modifica l'obiettivo di @appliance",
    "modifica l'obiettivo per @appliance",
    "cambia l'obiettivo di @appliance",
    "cambia l'obiettivo per @appliance",
    "modifica il goal di @appliance",
    "modifica il goal per @appliance",
    "cambia il goal di @appliance",
    "cambia il goal per @appliance",
  ],
};

class EditGoalIntent extends Intent {
  constructor(nlpManager) {
    super(
      strings.editGoalIntent,
      utterances[strings.currentLanguage],
      [],
      nlpManager
    );
  }

  async intentAction(
    nlpAnswer,
    dialogManager,
    contextManager,
    intents,
    chatbotManager
  ) {
    // extract from the nlpAnswer the appliance name
    let currentAppliance;
    let currentApplianceText;
    for (const entity of nlpAnswer.entities) {
      if (entity.entity === strings.applianceEntity) {
        currentAppliance = entity.option;
        currentApplianceText = entity.sourceText;
      }
    }

    // if the appliance name was specified, we can continue. Otherwise, the slotFilling will ask the user to specify the appliance name, and we can close the intentAction
    if (!currentAppliance) {
      return;
    }

    // we can assume that the db is already filled with the list of all appliance in the appliances table
    const previousConsumptionsData = await loadConsumptionData(
      currentAppliance
    );

    // check if a goal is already set for the appliance
    if (!previousConsumptionsData.data.goal) {
      let generatedAnswer;
      if (strings.currentLanguage === "en") {
        generatedAnswer =
          "You still don't have a goal set for " +
          currentApplianceText +
          ". do you want to set a goal for it now? ";
      } else if (strings.currentLanguage === "it") {
        generatedAnswer =
          "Non hai ancora impostato un goal per " +
          currentApplianceText +
          ". Vuoi impostarne uno adesso? ";
      }
      // for that appliance, there is no goal set.
      chatbotManager.printChatbotAnswer(generatedAnswer);

      // put in the context the set goal intent, in order to set a goal if the users answers yes in the yesNoIntent
      ContextManager.prototype.continuePreviousIntent = async function () {
        let generatedAnswer;
        if (strings.currentLanguage === "en") {
          generatedAnswer = "set a goal for " + currentApplianceText;
        } else if (strings.currentLanguage === "it") {
          generatedAnswer = "imposta un goal per " + currentApplianceText;
        }
        await chatbotManager
          .answerToUser(generatedAnswer, false)
          .then(() => {});
      };

      // set the continuePreviousIntent action as valid (for 1 intent)
      contextManager.continuePreviousIntentTokens = 2;
    } else {
      let generatedAnswer;
      if (strings.currentLanguage === "en") {
        generatedAnswer =
          "Okay! The goal for " +
          currentApplianceText +
          " is too difficult or too easy?";
      } else if (strings.currentLanguage === "it") {
        generatedAnswer =
          "Ok! Il goal per " +
          currentApplianceText +
          " è troppo difficile o troppo facile?";
      }
      chatbotManager.printChatbotAnswer(generatedAnswer);
    }
  }
}

module.exports = EditGoalIntent;
