const Intent = require("./Intent");
const strings = require("../../data/strings");
const { loadAllConsumptions } = require("../../utils/makeAPICall");

/**
 * This intent can be used by the user to know the list of the appliances currently installed
 * (ie: with a goal, a current consumption OR a past consumption).
 */

const utterances = {
  en: [
    "what my appliances?",
    "which appliances do i have?",
    "list my appliances",
    "appliances list",
  ],
  it: [
    "quali sono elettrodomestici?",
    "quali elettrodomestici ho?",
    "elenca elettrodomestici",
    "elenca elettrodomestici",
    "dispositivi configurati",
    "dispositivi collegati",
    "dispositivi installati",
    "elettrodomestici configurati",
    "elettrodomestici collegati",
    "elettrodomestici installati",
    "che cosa ho configurato",
    "che cosa ho collegato",
    "che cosa ho installato",
    "che cosa è acceso",
    "la @appliance è acceso",
    "la @appliance è spento",
    "la @appliance è accesa",
    "la @appliance è spenta",
  ],
};

class ListAppliancesIntent extends Intent {
  constructor(nlpManager) {
    super(
      strings.listAppliancesIntent,
      utterances[strings.currentLanguage],
      [],
      nlpManager
    );
  }

  async intentAction(
    nlpAnswer,
    dialogManager,
    contextManager,
    intents,
    chatbotManager
  ) {
    const userConsumptionsData = await loadAllConsumptions();

    const appliancesConfigured = [];

    if (userConsumptionsData) {
      userConsumptionsData.data.forEach((consumption) => {
        if (
          (consumption.currentConsumptions ||
            consumption.averageUserConsumptions ||
            consumption.goal) &&
          consumption.applianceName !== strings.entirehouseAppliance.id
        ) {
          appliancesConfigured.push(
            strings[
              consumption.applianceName.toLowerCase().replace(/\s/g, "") +
                "Appliance"
            ][strings.currentLanguage][0]
          );
        }
      });
    } else {
      const answer = {
        en: "Currently, you don't have any appliance configured",
        it: "Attualmente, non hai nessun elettrodomestico configurato",
      };
      chatbotManager.printChatbotAnswer(answer[strings.currentLanguage]);
      return;
    }

    if (appliancesConfigured.length === 0) {
      {
        const answer = {
          en: "Currently, you don't have any appliance configured",
          it: "Attualmente, non hai nessun elettrodomestico configurato",
        };
        chatbotManager.printChatbotAnswer(answer[strings.currentLanguage]);
        return;
      }
    }

    let finalAnswer;
    if (strings.currentLanguage === "en") {
      finalAnswer = "You have configured: ";
    } else if (strings.currentLanguage === "it") {
      finalAnswer = "Hai configurato: ";
    }

    appliancesConfigured.forEach((appliance, index) => {
      finalAnswer += appliance;
      if (index === appliancesConfigured.length - 2) {
        if (strings.currentLanguage === "en") {
          finalAnswer += " and ";
        } else if (strings.currentLanguage === "it") {
          finalAnswer += " e ";
        }
      } else if (index < appliancesConfigured.length - 1) {
        finalAnswer += ", ";
      }
    });

    chatbotManager.printChatbotAnswer(finalAnswer);
  }
}

module.exports = ListAppliancesIntent;
