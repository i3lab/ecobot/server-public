const Intent = require("./Intent");
const strings = require("../../data/strings");

/**
 * This is the intent triggered automatically by nlp.js when the utterance doesn't
 * match any other intent.
 */

const answers = {
  en: ["Sorry, I don't understand"],
  it: [
    "Scusa, non ho capito. Prova ad utilizzare delle parole diverse, grazie!",
    "Mi dispiace ma non ho capito. Puoi ripeterlo in modo diverso per aiutarmi a capire?",
    "Scusa, non ho capito. Se vuoi conoscere che cosa posso fare, scrivimi 'aiutami'",
    "Mi dispiace, non ho capito. Scrivimi 'che cosa puoi fare' per sapere che cosa posso fare",
    "Non ho capito bene! Prova a chiedermelo in un altro modo, grazie!",
  ],
};
class NoneIntent extends Intent {
  constructor(nlpManager) {
    super(strings.noneIntent, [], answers[strings.currentLanguage], nlpManager);
  }

  intentAction() {}
}

module.exports = NoneIntent;
