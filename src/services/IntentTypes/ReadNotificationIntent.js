const Intent = require("./Intent");
const strings = require("../../data/strings");
const ContextManager = require("../ContextManager");

/**
 * This intent allows the user to read a notification. It's triggered both with
 * the specified utterances, both when the user presses the 'X new notifications' in
 * the frontend (that mimics the utterance 'read notifications').
 *
 * The action:
 * - check if there are new notifications
 * - if there are, fetches the one with the highest priority
 * - it will read this notification, remove it from the list of new notifications
 * - it will ask the user if they want to set a goal for the appliance the notification was referring to
 */

const utterances = {
  en: [
    "read notifications",
    "see notifications",
    "open notifications",
    "read notification",
    "see notification",
    "open notification",
  ],
  it: ["notifiche", "apri notifiche", "vedi notifiche"],
};

class ReadNotificationIntent extends Intent {
  constructor(nlpManager) {
    super(
      strings.readNotificationIntent,
      utterances[strings.currentLanguage],
      [],
      nlpManager
    );
  }

  intentAction(
    nlpAnswer,
    dialogManager,
    contextManager,
    intents,
    chatbotManager,
    notificationManager
  ) {
    // Check if there are notifications
    if (notificationManager.notificationsNumber() > 0) {
      const notificationWithHighestPriority =
        notificationManager.getNotificationWithHighestPriority();
      chatbotManager.printChatbotAnswer(
        notificationWithHighestPriority.message
      );

      // Include in the context the second part of the ReadNotificationIntent action, that will be executed by the
      // YesNoIntent just if the user agrees (i.e.: if the user answers 'yes'). For example, could be to set a goal
      ContextManager.prototype.continuePreviousIntent = async function () {
        const utterance = intents.find(
          (i) => i.intentName === notificationWithHighestPriority.type // The intent with the name as the notification type will be simulated. For example, if the notification type is 'goal.set', the intent 'goal.set' will be simulated
        )?.utterances[0]; // get the first utterance of the right intent (the one with the name as the notification type)
        const applianceWithAt = "@" + strings.applianceEntity; // = @appliance: if the utterance of the intent contains @appliance (ie, can be relative to a specific appliance), it will be substituted with the appliance name specified in the notification

        if (utterance) {
          // notification type is valid, because it corresponds to a valid intent: simulate the intent
          await chatbotManager
            .answerToUser(
              utterance.replace(
                applianceWithAt,
                strings[
                  notificationWithHighestPriority.appliance
                    .toLowerCase()
                    .replace(/\s/g, "") + "Appliance"
                ][strings.currentLanguage][0]
              ), // as if the user had said the utterance with the appliance name
              false
            )
            .then(() => {});
        } else {
          // notification is invalid. There is no intent that can execute it
          const generatedAnswer = {
            en: "I'm sorry! I don't know what to do to help you with this notification. Please, contact the developers.",
            it: "Mi dispiace! Non so come aiutarti con questa notifica. Per favore, contatta gli sviluppatori. ",
          };
          chatbotManager.printChatbotAnswer(
            generatedAnswer[strings.currentLanguage]
          );
        }
      };

      // set the continuePreviousIntent action as valid (for 1 intent)
      contextManager.continuePreviousIntentTokens = 2;

      // After the notification has been read, remove it from the queue
      notificationManager.removeNotification(
        notificationWithHighestPriority.ID
      );

      return;
    }

    const generatedAnswer = {
      en: "Currently, you have no notifications. Yuppie!",
      it: "Attualmente, non hai nessuna notifica. Evviva!",
    };
    chatbotManager.printChatbotAnswer(generatedAnswer[strings.currentLanguage]);
  }
}

module.exports = ReadNotificationIntent;
