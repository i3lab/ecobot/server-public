const Intent = require("./Intent");
const strings = require("../../data/strings");
const {
  loadConsumptionData,
  updateConsumption,
} = require("../../utils/makeAPICall");
const ContextManager = require("../ContextManager");

/**
 * This intent allows the user to remove a goal for an appliance.
 */

const utterances = {
  en: [
    "remove the goal for @appliance",
    "delete the goal for @appliance",
    "remove a goal for @appliance",
    "delete a goal for @appliance",
    "remove the @appliance goal",
    "delete the @appliance goal",
  ],
  it: [
    "rimuovi il goal per @appliance",
    "cancella il goal per @appliance",
    "elimina il goal per @appliance",
    "annulla il goal per @appliance",
    "rimuovi l'obiettivo per @appliance",
    "cancella l'obiettivo per @appliance",
    "elimina l'obiettivo per @appliance",
    "annulla l'obiettivo per @appliance",
  ],
};

class RemoveGoalIntent extends Intent {
  constructor(nlpManager) {
    super(
      strings.removeGoalIntent,
      utterances[strings.currentLanguage],
      [],
      nlpManager
    );
  }

  async intentAction(
    nlpAnswer,
    dialogManager,
    contextManager,
    intents,
    chatbotManager
  ) {
    // extract from the nlpAnswer the appliance name
    let currentAppliance;
    let currentApplianceText;
    for (const entity of nlpAnswer.entities) {
      if (entity.entity === strings.applianceEntity) {
        currentAppliance = entity.option;
        currentApplianceText = entity.sourceText;
      }
    }
    // if the appliance name was specified, we can continue. Otherwise, the slotFilling will ask the user to specify the appliance name, and we can close the intentAction
    if (!currentAppliance) {
      return;
    }

    // we can assume that the db is already filled with the list of all appliance in the appliances table
    const previousConsumptionsData = await loadConsumptionData(
      currentAppliance
    );

    // check if a goal is already set for the appliance
    if (!previousConsumptionsData.data.goal) {
      // for that appliance, there is no goal set.
      const generatedAnswer = {
        en: "There is already no goal set for " + currentApplianceText + "!",
        it: "Non hai nessun goal impostato per " + currentApplianceText + "!",
      };
      chatbotManager.printChatbotAnswer(
        generatedAnswer[strings.currentLanguage]
      );
    } else {
      // ask a confirmation to the user
      const generatedAnswer = {
        en:
          "Do you really want to remove the goal of " +
          Math.round(previousConsumptionsData.data.goal) +
          " kWh for " +
          currentApplianceText +
          "? Remember that, if it's too complex to reach, you can also decrease its difficulty.",
        it:
          "Vuoi davvero rimuovere il goal di " +
          Math.round(previousConsumptionsData.data.goal) +
          " kWh per " +
          currentApplianceText +
          "? Ricordati che, se è troppo difficile da raggiungere, puoi anche diminuirne la difficoltà.",
      };
      chatbotManager.printChatbotAnswer(
        generatedAnswer[strings.currentLanguage]
      );

      // put in the context the actual goal removal function. It will be executed if the user confirms the goal removal
      ContextManager.prototype.continuePreviousIntent = async function () {
        await updateConsumption(
          currentAppliance,
          previousConsumptionsData.data.currentConsumptions,
          previousConsumptionsData.data.averageGlobalConsumptions,
          previousConsumptionsData.data.averageUserConsumptions,
          null, // remove the goal
          null // remove the goal period
        );

        const generatedAnswer = {
          en: "Got it. I've removed the goal for " + currentApplianceText,
          it: "Ecco fatto. Ho rimosso il goal per " + currentApplianceText,
        };
        chatbotManager.printChatbotAnswer(
          generatedAnswer[strings.currentLanguage]
        );
      };

      // set the continuePreviousIntent action as valid (for 1 intent)
      contextManager.continuePreviousIntentTokens = 2;
    }
  }
}

module.exports = RemoveGoalIntent;
