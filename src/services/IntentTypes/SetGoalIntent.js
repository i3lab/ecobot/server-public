const Intent = require("./Intent");
const strings = require("../../data/strings");
const {
  loadConsumptionData,
  updateConsumption,
} = require("../../utils/makeAPICall");
const ContextManager = require("../ContextManager");

/**
 * This intent allows the user to set a goal for a specific appliance.
 * It will also check if the goal is already set and ask the user if he wants to edit it.
 */

const utterances = {
  en: [
    "set a goal for @appliance",
    "set the goal for @appliance",
    "add a goal for @appliance",
    "add the goal for @appliance",
    "create a goal for @appliance",
    "create the goal for @appliance",
  ],
  it: [
    "imposta un goal per @appliance",
    "imposta il goal per @appliance",
    "setta un goal per @appliance",
    "setta il goal per @appliance",
    "aggiungi un goal per @appliance",
    "aggiungi il goal per @appliance",
    "crea un goal per @appliance",
    "crea il goal per @appliance",
    "imposta un obiettivo per @appliance",
    "aggiungi un obiettivo per @appliance",
    "crea un obiettivo per @appliance",
    "aiutami a risparmiare per @appliance",
    "fammi risparmiare per @appliance",
    "voglio risparmiare per @appliance",
    "limita il consumo di @appliance",
    "limita il consumo per @appliance",
    "come posso impostare un goal di risparmio energetico per @appliance",
    "come posso monitorare i miei consumi energetici per @appliance",
    "monitorare i consumi per @appliance",
    "voglio monitorare i consumi per @appliance",
    "puoi impostare un goal per @appliance",
    "puoi aggiungere un goal per @appliance",
    "puoi settare un goal per @appliance",
  ],
};

class SetGoalIntent extends Intent {
  constructor(nlpManager) {
    super(
      strings.setGoalIntent,
      utterances[strings.currentLanguage],
      [],
      nlpManager
    );
  }

  async intentAction(
    nlpAnswer,
    dialogManager,
    contextManager,
    intents,
    chatbotManager
  ) {
    // extract from the nlpAnswer the appliance name
    let currentAppliance;
    let currentApplianceText;
    for (const entity of nlpAnswer.entities) {
      if (entity.entity === strings.applianceEntity) {
        currentAppliance = entity.option;
        currentApplianceText = entity.sourceText;
      }
    }

    // if the user has specified the goal value (ie: set a goal of 50 for television, set a goal of 50 kwh for television),
    // we can retrieve it from the nlpAnswer and memorise it to set the correct value. Otherwise, a default value (based
    // on the usage statistics saved in the database) will be used.
    let userSpecifiedGoal = null;
    for (const entity of nlpAnswer.entities) {
      if (entity.entity === strings.numberEntity) {
        userSpecifiedGoal = Math.round(entity.resolution.value);
      }
    }

    if (userSpecifiedGoal !== null && userSpecifiedGoal < 1) {
      const generatedAnswer = {
        en: "Please, set a goal of at least 1 kwh. Otherwise, it's impossible to reach it!",
        it: "Per favore, imposta un obiettivo di almeno 1 kwh. Altrimenti, non è possibile raggiungerlo!",
      };
      chatbotManager.printChatbotAnswer(
        generatedAnswer[strings.currentLanguage]
      );

      return;
    }

    // if the appliance name was specified, we can continue. Otherwise, the slotFilling will ask the user to specify the appliance name, and we can close the intentAction
    if (!currentAppliance) {
      return;
    }

    // we can assume that the db is already filled with the list of all appliance in the appliances table
    const previousConsumptionsData = await loadConsumptionData(
      currentAppliance
    );
    let newGoal = null;

    // check if a goal is already set for the appliance
    if (!previousConsumptionsData.data.goal) {
      // this means that a goal was not already set for the appliance.
      if (userSpecifiedGoal !== null) {
        // if the user has specified the goal value, we can set it directly
        newGoal = userSpecifiedGoal;

        const generatedAnswer = {
          en:
            "Okay! Setting a goal of " +
            newGoal +
            " kWh per month for " +
            currentApplianceText +
            ". You can edit it later if you want.",
          it:
            "Ho settato un goal di " +
            newGoal +
            " kWh al mese per " +
            currentApplianceText +
            ". Puoi modificarlo in ogni momento, se vuoi.",
        };
        chatbotManager.printChatbotAnswer(
          generatedAnswer[strings.currentLanguage]
        );
      } else {
        // We can set the default one, putting the end of period in current_date + 31 days
        // the default goal will be: equal to averageUserConsumptions - 10% if we have this value (ie the user has been
        // using the system at least for 1 month, so we can make an average of their consumptions), otherwise it will be
        // equal to averageGlobalConsumptions - 10% (so, if we don't know how much the user consumes, we will set the goal based on global data)
        if (previousConsumptionsData.data.averageUserConsumptions) {
          newGoal = Math.round(
            previousConsumptionsData.data.averageUserConsumptions * 0.9
          );

          const generatedAnswer = {
            en:
              "Okay! Setting a goal for " +
              currentApplianceText +
              ": usually, you consume " +
              previousConsumptionsData.data.averageUserConsumptions +
              " kWh per month. I will set the goal to " +
              newGoal +
              " kWh per month. You can edit it later if you want.",
            it:
              "Ho settato un goal per " +
              currentApplianceText +
              ": di solito, consumi " +
              previousConsumptionsData.data.averageUserConsumptions +
              " kWh al mese. Ho settato il goal a " +
              newGoal +
              " kWh per mese. Puoi modificarlo in ogni momento, se vuoi.",
          };
          chatbotManager.printChatbotAnswer(
            generatedAnswer[strings.currentLanguage]
          );
        } else {
          newGoal = Math.round(
            previousConsumptionsData.data.averageGlobalConsumptions * 0.9
          ); // we can assume that the averageGlobalConsumptions data is always present, since it's always added during the initialisation
          const generatedAnswer = {
            en:
              "Okay! Setting a goal for " +
              currentApplianceText +
              ": usually, an average user consume " +
              previousConsumptionsData.data.averageGlobalConsumptions +
              " kWh per month. I will set the goal to " +
              newGoal +
              " kWh per month. You can edit it later if you want.",
            it:
              "Ho settato un goal per " +
              currentApplianceText +
              ": un utente, in media, consuma " +
              previousConsumptionsData.data.averageGlobalConsumptions +
              " kWh al mese. Ho settato il goal a " +
              newGoal +
              " kWh per mese. Puoi modificarlo in ogni momento, se vuoi.",
          };
          chatbotManager.printChatbotAnswer(
            generatedAnswer[strings.currentLanguage]
          );
        }
      }

      const currentDate = new Date(); // today's date
      const newGoalPeriodEnd = new Date(
        currentDate.setDate(currentDate.getDate() + 30)
      ); // today's date + 30 days: the goal period will end 30 days from when is set

      // we can now save the new goal in the database
      await updateConsumption(
        currentAppliance,
        previousConsumptionsData.data.currentConsumptions,
        previousConsumptionsData.data.averageGlobalConsumptions,
        previousConsumptionsData.data.averageUserConsumptions,
        newGoal,
        newGoalPeriodEnd.toISOString().slice(0, 10) // this is the current date + 1 month in the "YYYY-MM-DD" format. Slice function is used to remove the time part
      );
    } else {
      const generatedAnswer = {
        en:
          "A goal for " +
          currentApplianceText +
          " of " +
          previousConsumptionsData.data.goal +
          " kWh per month is already set. Do you want to edit it?",
        it:
          "Un goal per " +
          currentApplianceText +
          " di " +
          previousConsumptionsData.data.goal +
          " kWh al mese è già stato settato. Vuoi modificarlo?",
      };
      chatbotManager.printChatbotAnswer(
        generatedAnswer[strings.currentLanguage]
      );
      // if the goal is already present, ask the user if they want to edit it (and, if they answer yes, launch the EditGoalIntent):
      // To do so, include in the context the goal.edit intent action, that will be executed by the
      // YesNoIntent just if the user agrees (i.e.: if the user answers 'yes').
      ContextManager.prototype.continuePreviousIntent = async function () {
        const generatedAnswer = {
          en: "edit a goal for " + currentApplianceText,
          it: "modifica l'obiettivo di " + currentApplianceText,
        };
        await chatbotManager
          .answerToUser(generatedAnswer[strings.currentLanguage], false)
          .then(() => {});
      };

      // set the continuePreviousIntent action as valid (for 1 intent)
      contextManager.continuePreviousIntentTokens = 2;
    }
  }
}

module.exports = SetGoalIntent;
