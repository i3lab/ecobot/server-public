const Intent = require("./Intent");
const strings = require("../../data/strings");
const {
  loadUserData,
  createNewUser,
  updateUser,
} = require("../../utils/makeAPICall");

/**
 * When an intent that requires the user area information is triggered, but the
 * chatbot doesn't have the area information yet, this intent is triggered.
 * If the user info already exists in the db, the updateUser function is called, otherwise
 * the createNewUser function is called.
 *
 * The user has also the possibility to specify the area information in every moment.
 */
class SpecifyRegionIntent extends Intent {
  constructor(nlpManager) {
    super(strings.specifyRegionIntent, ["@region"], [], nlpManager);
  }

  async intentAction(
    nlpAnswer,
    dialogManager,
    contextManager,
    intents,
    chatbotManager
  ) {
    // I load the user data. If the user does not exist I create it, otherwise, I update it with the new area.
    const userData = await loadUserData();

    let newRegion;
    for (const entity of nlpAnswer.entities) {
      if (entity.entity === strings.regionEntity) {
        newRegion = entity.option;
      }
    }

    if (!userData) {
      await createNewUser(null, null, newRegion).then(async () => {
        await this.afterDataUpdate(
          nlpAnswer,
          dialogManager,
          contextManager,
          intents,
          chatbotManager,
          newRegion
        );
      });
    } else {
      await updateUser(
        userData.data.name, // copy old info
        userData.data.surname, // copy old info
        newRegion // new area information. So, through this intent, the user can update the area information
      ).then(async () => {
        await this.afterDataUpdate(
          nlpAnswer,
          dialogManager,
          contextManager,
          intents,
          chatbotManager,
          newRegion
        );
      });
    }
  }

  async afterDataUpdate(
    nlpAnswer,
    dialogManager,
    contextManager,
    intents,
    chatbotManager,
    newRegion
  ) {
    // In some cases, the previous intent action should be launched again to continue the process. Those cases must be
    // hardcoded since the user can also specify the origin region without being in another intent process.
    // This is the only way to avoid a state machine with a 'specifyAreaState'.
    const previousIntent = dialogManager.previousIntent;

    if (previousIntent === strings.areaIntent) {
      await intents
        .find((i) => i.intentName === previousIntent)
        ?.intentAction(
          nlpAnswer,
          dialogManager,
          contextManager,
          intents,
          chatbotManager
        ); // continue the areaIntent process, but now the information of the area is available!
    } else {
      const computedAnswer = {
        en: "Okay! I'll remember that you live in " + newRegion + ".",
        it: "Okay! Mi ricorderò che abiti in " + newRegion + ".",
      };
      await chatbotManager.printChatbotAnswer(
        computedAnswer[strings.currentLanguage]
      ); // The user just wanted to specify a new region. Print an answer.
    }
  }
}

module.exports = SpecifyRegionIntent;
