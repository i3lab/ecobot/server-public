const Intent = require("./Intent");
const strings = require("../../data/strings");
const {
  loadSuggestionsForAppliance,
  loadSuggestionsGeneral,
} = require("../../utils/makeAPICall");

/**
 * This intent allows the user to request a general suggestion or a suggestion
 * for a specific appliance.
 */

const utterances = {
  en: [
    "give me a suggestion for @appliance",
    "suggestion for @appliance",
    "please provide me a suggestion for @appliance",
    "how can I save more energy for @appliance?",
  ],
  it: [
    "dammi un suggerimento per @appliance",
    "suggerimento per @appliance",
    "dammi un suggerimento @appliance",
    "per favore, dammi un suggerimento per @appliance",
    "dammi un consiglio per @appliance",
    "dammi dei consigli per consumare di meno per @appliance",
    "dammi dei consigli per risparmiare per @appliance",
    "dammi dei consigli per risparmiare energia per @appliance",
    "dammi suggerimenti per @appliance",
    "suggerimenti @appliance",
    "suggerimento @appliance",
    "dammi suggerimento @appliance",
    "consiglio per @appliance",
    "per favore, dammi un consiglio per @appliance",
    "come posso risparmiare più energia per @appliance?",
    "cosa posso fare per risparmiare per @appliance",
    "come posso fare per consumare di meno per @appliance",
    "come consumare di meno per @appliance",
    "come posso ridurre i consumi per @appliance",
    "come faccio a risparmiare quando uso @appliance",
    "perchè sto consumando troppo per @appliance",
    "dammi un altro suggerimento per @appliance",
    "altro suggerimento per @appliance",
    "altro per @appliance",
    "suggerimento @appliance",
    "consiglio @appliance",
    "come posso risparmiare energia per @appliance",
    "come posso risparmiare energia con @appliance",
    "suggeriscimi come risparmiare",
    "come posso fare a risparmiare",
    "cosa posso fare per risparmiare",
    "come si risparmia energia",
    "come posso limitare il consumo di @appliance",
    "come posso limitare il consumo della @appliance",
    "come posso limitare il consumo @appliance",
    "limitare il consumo di @appliance",
    "cosa suggerisci per limitare consumo @appliance",
    "un altro",
    "come mai sto consumando troppo per @appliance",
    "come posso risparmiare di piu quando uso @appliance",
    "quali sono i fattori che incidono sui consumi energetici di @appliance",
    "quali sono i migliori consigli per risparmiare energia in @appliance?",
    "quali sono i consigli per risparmiare energia con la mia @appliance?",
    "quali sono i migliori consigli @appliance",
    "quali sono i consigli @appliance",
    "quali consigli mi puoi dare per @appliance",
    "quali consigli hai per @appliance",
    "come posso ottenere il massimo risparmio energetico dal mio @appliance",
    "come posso ottenere il massimo risparmio energetico dalla mia @appliance",
    "quali sono i trucchi per risparmiare energia con @appliance",
    "quali sono i trucchi per risparmiare energia con la @appliance",
    "trucchi per risparmiare energia con @appliance",
    "trucchi per risparmiare energia con la @appliance",
    "trucchi per risparmiare energia con il @appliance",
    "cosa posso fare per diminuire i consumi energetici @appliance?",
    "come posso fare per abbassare i consumi energetici",
    "dammi un suggerimento per la mia @appliance",
    "come posso risparmiare quando uso @appliance",
    "come posso risparmiare nell'uso di @appliance",
    "come posso risparmiare se uso @appliance",
    "come limitare il consumo di @appliance",
    "cosa posso fare @appliance",
    "che cosa posso fare @appliance?",
    "fammi risparmiare energia per @appliance",
    "fammi risparmiare energia @appliance",
    "fammi risparmiare energia con @appliance",
    "dammi consigli di risparmio per @appliance",
    "ancora",
  ],
};

class SuggestionIntent extends Intent {
  constructor(nlpManager) {
    super(
      strings.suggestionIntent,
      utterances[strings.currentLanguage],
      [],
      nlpManager
    );
  }

  printSuggestion(suggestionsList, chatbotManager, applianceName) {
    if (suggestionsList.data.length > 0) {
      // if we found suggestions, we will send one of them, randomly
      const randomSuggestion =
        suggestionsList.data[
          Math.floor(Math.random() * suggestionsList.data.length)
        ].content;
      const introText = {
        en: "Here's a suggestion for you: ",
        it: "Ecco un suggerimento per te: ",
      };
      chatbotManager.printChatbotAnswer(
        introText[strings.currentLanguage] + randomSuggestion
      );
    } else {
      // no suggestions are available for that appliance.
      if (applianceName) {
        const generatedAnswer = {
          en:
            "Currently, I don't have any suggestion for you for " +
            applianceName +
            ". Try asking me a general suggestion (by asking 'give me a suggestion for my house), or a suggestion for a different appliance.",
          it:
            "Attualmente, non ho nessun suggerimento da darti per " +
            applianceName +
            ". Prova a chiedermi un suggerimento generale (chiedendo 'dammi un suggerimento per la mia casa'), o un suggerimento per un altro elettrodomestico.",
        };
        chatbotManager.printChatbotAnswer(
          generatedAnswer[strings.currentLanguage]
        );
      } else {
        const generatedAnswer = {
          en: "Currently, I don't have any general suggestion for you. Try asking me for a suggestion for a specific appliance.",
          it: "Attualmente, non ho nessun suggerimento generale da darti. Prova a chiedermi un suggerimento per un elettrodomestico specifico.",
        };
        chatbotManager.printChatbotAnswer(
          generatedAnswer[strings.currentLanguage]
        );
      }
    }
  }

  async intentAction(
    nlpAnswer,
    dialogManager,
    contextManager,
    intents,
    chatbotManager
  ) {
    let applianceName;
    let applianceNameText;
    for (const entity of nlpAnswer.entities) {
      if (entity.entity === strings.applianceEntity) {
        applianceName = entity.option;
        applianceNameText = entity.sourceText;
      }
    }

    // if the appliance name was specified, we can continue. Otherwise, the slotFilling will ask the user to specify the appliance name, and we can close the intentAction
    if (!applianceName) {
      return;
    }

    // if the user wants a suggestion for the entire house, we will load the general suggestions: a general suggestion does not have an appliance associated, so it's sufficient to set applianceName and applianceNameText to null
    if (applianceName === strings.entirehouseAppliance.id) {
      applianceName = null;
      applianceNameText = null;
    }

    if (applianceName) {
      // if the user specified an appliance, we will search for a suggestion for that appliance
      const applianceName = nlpAnswer.entities[0].option;
      const curiosities = await loadSuggestionsForAppliance(applianceName);
      this.printSuggestion(curiosities, chatbotManager, applianceNameText);
    } else {
      const curiosities = await loadSuggestionsGeneral();
      this.printSuggestion(curiosities, chatbotManager, null);
    }
  }
}

module.exports = SuggestionIntent;
