const Intent = require("./Intent");
const strings = require("../../data/strings");

/**
 * This intent will answer to the user with a generic thank-you message.
 */

const utterances = {
  en: ["thanks you", "thanks", "many thanks", "thank you"],
  it: ["grazie", "grazie mille", "molte grazie"],
};

const answers = {
  en: ["You're welcome. It's my pleasure to help you!"],
  it: ["Di nulla. È un piacere aiutarti!"],
};

class ThanksIntent extends Intent {
  constructor(nlpManager) {
    super(
      strings.thanksIntent,
      utterances[strings.currentLanguage],
      answers[strings.currentLanguage],
      nlpManager
    );
  }

  intentAction() {}
}

module.exports = ThanksIntent;
