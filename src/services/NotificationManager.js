/**
 * This class manages chatbot notifications. Notifications are messages that the chatbot sends to the user
 * in order to convince them to solve energy problems by setting some goals. For example, a 'area' notification
 * can let the user know that for a specific appliance they are consuming more energy that the average in their area.
 * After that the notification is read, it's removed from the list of notifications and the chatbot asks to the user
 * of they want to set a goal for that appliance.
 *
 * Note: by default, notifications are not automatically sent to the user immediately after they are generated! The user
 * can read them with the ReadNotificationIntent or by pressing a specific button in the frontend, that appear when
 * there is at least 1 notification in the pool. This behaviour is meant to avoid interrupting the user and spamming them with
 * notifications.
 *
 * Note 2: currently, the triggers that will be used in production to generate notifications are not available. Some mock
 * notifications can be generated in the frontend by pressing some buttons, and everything will work as expected.
 *
 * Note 3: every notification has a priority. The higher the priority number, the more important the notification is.
 * When the user ask to read the notification, the chatbot will read the notification with the highest priority and remove
 * it from the list. So, the next time, the chatbot will read the notification with the second highest priority, and so on.
 *
 * Note 4: notifications are managed through API. The API allows to read the notification with the highest priority,
 * know the number of notifications, create and remove a new notification (with some parameters).
 */
class NotificationManager {
  constructor() {
    // currently, chatbot notifications are just for testing purposes and can be included in this list by the frontend
    // since some real triggers are not available yet, notification list is saved just in RAM and is cleaned after every restart
    this._notifications = [];
  }

  get notifications() {
    // return all notifications
    return this._notifications;
  }

  getNotificationWithHighestPriority() {
    // find the notification with the highest priority. Used in the ReadNotificationIntent to avoid loading all notifications
    return this.notifications.reduce(function (prev, current) {
      return prev.priority > current.priority ? prev : current;
    });
  }

  addNotification(type, appliance, priority, message) {
    const newID = this._notifications.length + 1; // generate automatically a new ID for the notification
    this._notifications.push({
      ID: newID, // ID of the notification. Useful to track and remove a notification after it has been read
      type: type, // Type can be, for example, area, goal, etc. In this way, it's possible to understand which strategy to use to convince the user to set a goal. E.g.: type = area -> suggest to set a goal to consume less than your area
      appliance: appliance, // Appliance the notification is referring to
      priority: priority, // Priority is a number. The first notification show is the one with the highest priority (the highest the better),
      message: message, // The message to show when the notification is read by the user. For example, "Your notification.appliance is consuming too much!"
    });

    return newID;
  }

  removeNotification(ID) {
    // Remove a notification by ID (for example, after it has been read)
    this._notifications = this._notifications.filter(
      (notification) => notification.ID !== ID
    );
  }

  // In the frontend is necessary to know if there is at least one notification
  // in the pool, and the exact number to show on the button.
  notificationsNumber() {
    return this._notifications.length;
  }
}

module.exports = NotificationManager;
