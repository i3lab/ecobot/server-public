const AreaIntent = require("../services/IntentTypes/AreaIntent");
const CheckGoalIntent = require("../services/IntentTypes/CheckGoalIntent");
const CuriosityIntent = require("../services/IntentTypes/CuriosityIntent");
const DecreaseGoalIntent = require("../services/IntentTypes/DecreaseGoalIntent");
const EditGoalIntent = require("../services/IntentTypes/EditGoalIntent");
const ExitIntent = require("../services/IntentTypes/ExitIntent");
const IncreaseGoalIntent = require("../services/IntentTypes/IncreaseGoalIntent");
const NoneIntent = require("../services/IntentTypes/NoneIntent");
const RemoveGoalIntent = require("../services/IntentTypes/RemoveGoalIntent");
const SetGoalIntent = require("../services/IntentTypes/SetGoalIntent");
const SpecifyApplianceIntent = require("../services/IntentTypes/SpecifyApplianceIntent");
const SpecifyRegionIntent = require("../services/IntentTypes/SpecifyRegionIntent");
const SuggestionIntent = require("../services/IntentTypes/SuggestionIntent");
const YesNoIntent = require("../services/IntentTypes/YesNoIntent");
const ReadNotificationIntent = require("../services/IntentTypes/ReadNotificationIntent");
const HelpIntent = require("../services/IntentTypes/HelpIntent");
const ListAppliancesIntent = require("../services/IntentTypes/ListAppliancesIntent");
const ThanksIntent = require("../services/IntentTypes/ThanksIntent");
const CostIntent = require("../services/IntentTypes/CostItntent");
const CheckConsumptionIntent = require("../services/IntentTypes/CheckConsumptionIntent");
const AgainIntent = require("../services/IntentTypes/AgainIntent");
const MostUsedApplianceIntent = require("../services/IntentTypes/MostUsedApplianceIntent");
const ManageDeviceIntent = require("../services/IntentTypes/ManageDeviceIntent");
const CompareWithAverageIntent = require("../services/IntentTypes/CompareWithAverageIntent");

/**
 * This function creates all the intents and adds them to the NLP manager for the training.
 * Moreover, this array is used to execute the intent actions when needed.
 */
function intentsCreator(nlpManager) {
  return [
    new AreaIntent(nlpManager),
    new CheckGoalIntent(nlpManager),
    new CuriosityIntent(nlpManager),
    new DecreaseGoalIntent(nlpManager),
    new EditGoalIntent(nlpManager),
    new ExitIntent(nlpManager),
    new IncreaseGoalIntent(nlpManager),
    new NoneIntent(nlpManager),
    new RemoveGoalIntent(nlpManager),
    new SetGoalIntent(nlpManager),
    new SpecifyApplianceIntent(nlpManager),
    new SpecifyRegionIntent(nlpManager),
    new SuggestionIntent(nlpManager),
    new YesNoIntent(nlpManager),
    new ReadNotificationIntent(nlpManager),
    new HelpIntent(nlpManager),
    new ListAppliancesIntent(nlpManager),
    new ThanksIntent(nlpManager),
    new CostIntent(nlpManager),
    new CheckConsumptionIntent(nlpManager),
    // new AgainIntent(nlpManager),
    new MostUsedApplianceIntent(nlpManager),
    new ManageDeviceIntent(nlpManager),
    new CompareWithAverageIntent(nlpManager),
  ];
}

module.exports = intentsCreator;
