const axios = require("axios");

/**
 * This file is used to collect in a single place all the API calls made through Axios.
 * This is done to avoid having to write the same call code multiple times.
 *
 * Please note that some calls are done directly in the frontend and are not included here.
 */

async function loadUserData() {
  return await axios
    .get("http://localhost:8080/api/users/1", {})
    .catch(function () {});
}

async function createNewUser(name, surname, region) {
  await axios
    .post("http://localhost:8080/api/users/", {
      name: name,
      surname: surname,
      region: region,
    })
    .catch(function () {});
}

async function updateUser(name, surname, region) {
  await axios
    .put("http://localhost:8080/api/users/1", {
      name: name,
      surname: surname,
      region: region,
    })
    .catch(function () {});
}

async function createNewArea(areaName, averageConsumptions) {
  await axios
    .post("http://localhost:8080/api/areas/", {
      areaName: areaName,
      averageConsumptions: averageConsumptions,
    })
    .catch(function () {});
}

async function loadAreaData(areaName) {
  return await axios
    .get("http://localhost:8080/api/areas/" + areaName, {})
    .catch(function () {});
}

async function createNewConsumption(
  applianceName,
  currentConsumptions,
  averageGlobalConsumptions,
  averageUserConsumptions,
  goal,
  goalPeriodEnd
) {
  await axios
    .post("http://localhost:8080/api/consumptions/", {
      applianceName: applianceName,
      currentConsumptions: currentConsumptions,
      averageGlobalConsumptions: averageGlobalConsumptions,
      averageUserConsumptions: averageUserConsumptions,
      goal: goal,
      goalPeriodEnd: goalPeriodEnd,
    })
    .catch(function () {});
}

async function updateConsumption(
  applianceName,
  currentConsumptions,
  averageGlobalConsumptions,
  averageUserConsumptions,
  goal,
  goalPeriodEnd
) {
  await axios
    .put("http://localhost:8080/api/consumptions/" + applianceName, {
      applianceName: applianceName,
      currentConsumptions: currentConsumptions,
      averageGlobalConsumptions: averageGlobalConsumptions,
      averageUserConsumptions: averageUserConsumptions,
      goal: goal,
      goalPeriodEnd: goalPeriodEnd,
    })
    .catch(function () {});
}

async function loadAllConsumptions() {
  return await axios
    .get("http://localhost:8080/api/consumptions/", {})
    .catch(function () {});
}

async function loadConsumptionData(applianceName) {
  return await axios
    .get("http://localhost:8080/api/consumptions/" + applianceName, {})
    .catch(function () {});
}

async function createNewSuggestion(applianceName, content, type) {
  await axios
    .post("http://localhost:8080/api/suggestions/", {
      applianceName: applianceName,
      content: content,
      type: type,
    })
    .catch(function () {});
}

async function loadSuggestionsForAppliance(applianceName) {
  return await axios
    .get("http://localhost:8080/api/suggestions/" + applianceName, {})
    .catch(function () {});
}

async function loadSuggestionsGeneral() {
  return await axios
    .get("http://localhost:8080/api/suggestions/general/all", {})
    .catch(function () {});
}

async function createNewCuriosity(applianceName, content, type) {
  await axios
    .post("http://localhost:8080/api/curiosities/", {
      applianceName: applianceName,
      content: content,
      type: type,
    })
    .catch(function () {});
}

async function loadCuriositiesForAppliance(applianceName) {
  return await axios
    .get("http://localhost:8080/api/curiosities/" + applianceName, {})
    .catch(function () {});
}

async function loadCuriositiesGeneral() {
  return await axios
    .get("http://localhost:8080/api/curiosities/general/all", {})
    .catch(function () {});
}

module.exports = {
  loadUserData,
  createNewUser,
  updateUser,
  createNewArea,
  createNewConsumption,
  updateConsumption,
  loadAllConsumptions,
  loadConsumptionData,
  loadAreaData,
  createNewSuggestion,
  createNewCuriosity,
  loadSuggestionsForAppliance,
  loadSuggestionsGeneral,
  loadCuriositiesForAppliance,
  loadCuriositiesGeneral,
};
