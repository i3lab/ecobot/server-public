const {
  createNewArea,
  createNewConsumption,
  createNewSuggestion,
  createNewCuriosity,
} = require("./makeAPICall");
const strings = require("../data/strings");

/**
 * This function was created just for testing purposes, until real data exists.
 * It creates some mock data with some average area and consumptions, and some goals.
 * During the dev phase, the DB is cleaned after each execution and the state is reset
 * to the data contained in this function. This process happens in the setUpServerAndDatabase function,
 * after the db is initialised with db.sequelize.sync
 */

async function mockDatabaseData() {
  await mockAreaData();
  await mockConsumptionData();
  await mockSuggestionsAndCuriosities();
}

async function mockAreaData() {
  // data source: ISTAT. Monthly average consumptions (kWh) for a given area
  await createNewArea(strings.abruzzoRegion, 239);
  await createNewArea(strings.basilicataRegion, 225);
  await createNewArea(strings.calabriaRegion, 268);
  await createNewArea(strings.campaniaRegion, 239);
  await createNewArea(strings.emiliaRomagnaRegion, 269);
  await createNewArea(strings.friuliVeneziaGiuliaRegion, 248);
  await createNewArea(strings.lazioRegion, 258);
  await createNewArea(strings.liguriaRegion, 242);
  await createNewArea(strings.lombardiaRegion, 239);
  await createNewArea(strings.marcheRegion, 239);
  await createNewArea(strings.moliseRegion, 250);
  await createNewArea(strings.piemonteRegion, 244);
  await createNewArea(strings.pugliaRegion, 233);
  await createNewArea(strings.sardegnaRegion, 334);
  await createNewArea(strings.siciliaRegion, 284);
  await createNewArea(strings.toscanaRegion, 265);
  await createNewArea(strings.trentinoAltoAdigeRegion, 214);
  await createNewArea(strings.umbriaRegion, 252);
  await createNewArea(strings.valDAostaRegion, 272);
  await createNewArea(strings.venetoRegion, 257);
  await createNewArea(strings.trentoRegion, 255);
  await createNewArea(strings.bolzanoRegion, 268);
}

async function mockConsumptionData() {
  // for testing purposes: if you want to initialise the database with some goals already set, you can do it by assigning the goal and the goalPeriodEnd as:
  // new Date(new Date().setDate(new Date().getDate() + X)) // where X is the number of days from today to the goalPeriodEnd (must be from 1 to 30).
  // in this way, the goal is always set in the future, regardless of the date when the server is started.

  await createNewConsumption(
    strings.televisionAppliance.id,
    15,
    20,
    25,
    null,
    null
  );
  await createNewConsumption(
    strings.dishwasherAppliance.id,
    35,
    30,
    40,
    null,
    null
  );
  await createNewConsumption(
    strings.washingmachineAppliance.id,
    20,
    40,
    45,
    null,
    null
  );
  await createNewConsumption(
    strings.driermachineAppliance.id,
    0,
    100,
    60,
    null,
    null
  );
  await createNewConsumption(
    strings.fridgeAppliance.id,
    49,
    50,
    70,
    null,
    null
  );
  await createNewConsumption(
    strings.airconditionerAppliance.id,
    50,
    40,
    80,
    null,
    null
  );
  await createNewConsumption(
    strings.microwaveAppliance.id,
    2,
    4,
    5,
    null,
    null
  );
  await createNewConsumption(
    strings.electricovenAppliance.id,
    20,
    30,
    25,
    null,
    null
  );
  await createNewConsumption(
    strings.inductionhobAppliance.id,
    5,
    30,
    20,
    null,
    null
  );
  await createNewConsumption(
    strings.boilerAppliance.id,
    55,
    50,
    65,
    null,
    null
  );
  await createNewConsumption(
    strings.entirehouseAppliance.id,
    251,
    394,
    400,
    null,
    null
  );
}

async function mockSuggestionsAndCuriosities() {
  let curiosity;
  let suggestion;

  suggestion = {
    it: "Basandosi sulla temperatura attuale (33°C), non è necessario impostare il climatizzatore al massimo. Puoi ridurne la potenza, per risparmiare fino al 20% di energia.",
    en: "Based on the current temperature (33°C), is not recommended to use the air conditioner at the maximum level. Try to set it to a lower level, you'll save up to 20% of energy.",
  };
  await createNewSuggestion(
    strings.airconditionerAppliance.id,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "Hai lasciato acceso il climatizzatore in tutta la casa. Spegnilo in tutte le stanze in cui non sei!",
    en: "You left the air conditioner on in the entire house. Turn it off in all the rooms where you are not!",
  };
  await createNewSuggestion(
    strings.airconditionerAppliance.id,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "Controlla di avere tutte le finestre ben chiuse, altrimenti il climatizzatore non funzionerà al meglio.",
    en: "Check that all the windows are well closed, otherwise the air conditioner will not work at its best.",
  };
  await createNewSuggestion(
    strings.airconditionerAppliance.id,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "Il tuo condizionatore è vecchio. Comprandone uno nuovo, potrai risparmiare fino al 50% di energia.",
    en: "Your air conditioner is old. By buying a new one, you can save up to 50% of energy.",
  };
  await createNewSuggestion(
    strings.airconditionerAppliance.id,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "La combinazione del condizionatore con un ventilatore da soffitto potrebbe sembrare controproducente, in realtà è un utile sistema che permetterà di ottenere refrigerio più velocemente e ti aiuterà a non abusare del tuo impianto.",
    en: "To save energy, use also a ceiling fan, together with the air conditioner: the combination of the air conditioner with a ceiling fan may seem counterproductive, in reality it is a useful system that will allow you to get cooling faster and will help you not to abuse your system.",
  };
  await createNewSuggestion(
    strings.airconditionerAppliance.id,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "La posizione di installazione del condizionatore è molto importante: la situazione ideale è installarlo in una posizione ombreggiata della casa.",
    en: "",
  };
  await createNewSuggestion(
    strings.airconditionerAppliance.id,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "Ricorda di avviare la lavastoviglie sempre a pieno carico. Altrimenti, sprechi molta energia.",
    en: "The dishwasher you just run, is full? Remember that starting a dishwasher not full, wastes energy.",
  };
  await createNewSuggestion(
    strings.dishwasherAppliance.id,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "Utilizza la modalità Eco della tua lavastoviglie quando possibile, invece che il programma intensivo!",
    en: "Have you tried to use the Eco mode of your dishwasher when possible, instead of the intensive program?",
  };
  await createNewSuggestion(
    strings.dishwasherAppliance.id,
    suggestion[strings.currentLanguage],
    strings.ecoModeType
  );

  suggestion = {
    it: "Se avvii subito la lavastoviglie, il programma intensivo non sarà più necessario perchè lo sporco non avrà il tempo di incrostarsi.",
    en: "If you do not wait too long to start the dishwasher, the intensive program will not be necessary because the dirt will not have time to stick.",
  };
  await createNewSuggestion(
    strings.dishwasherAppliance.id,
    suggestion[strings.currentLanguage],
    strings.ecoModeType
  );

  suggestion = {
    it: "Non sciacquare i piatti prima di metterli nella lavastoviglie. Il programma di lavaggio è in grado di rimuovere lo sporco anche senza il prelavaggio.",
    en: "Do not rinse the dishes before putting them in the dishwasher. The washing program is able to remove dirt even without pre-washing.",
  };
  await createNewSuggestion(
    strings.dishwasherAppliance.id,
    suggestion[strings.currentLanguage],
    strings.ecoModeType
  );

  suggestion = {
    it: "Se utilizzi un programma di risparmio energetico nella lavastoviglie, i piatti vengono lavati a una temperatura inferiore, intorno ai 50° C, utilizzando circa tre litri di acqua in meno rispetto al normale ciclo.",
    en: "",
  };
  await createNewSuggestion(
    strings.dishwasherAppliance.id,
    suggestion[strings.currentLanguage],
    strings.ecoModeType
  );

  suggestion = {
    it: "Scegli una lavastoviglie dalla giusta dimensione, per ottimizzare al massimo l'utilizzo di acqua ed energia.",
    en: "",
  };
  await createNewSuggestion(
    strings.dishwasherAppliance.id,
    suggestion[strings.currentLanguage],
    strings.ecoModeType
  );

  suggestion = {
    it: "Spegni subito la TV quando smetti di guardarla, senza scordartela accesa o in stand-by.",
    en: "Are you still watching TV? If not, turn it off!",
  };
  await createNewSuggestion(
    strings.televisionAppliance.id,
    suggestion[strings.currentLanguage],
    strings.ecoModeType
  );

  suggestion = {
    it: "Quando spegni la TV, ricordati di staccare anche la spina: infatti, se la lasci in standby, continuerà a sprecare energia.",
    en: "When you turn off the TV, remember to unplug it: in fact, if you leave it in standby, it will continue to waste energy.",
  };
  await createNewSuggestion(
    strings.televisionAppliance.id,
    suggestion[strings.currentLanguage],
    strings.ecoModeType
  );

  suggestion = {
    it: "Anche la luce rossa della TV spreca energia. Quando la spegni, stacca anche la spina!",
    en: "Even the red light of the TV wastes energy. When you turn it off, unplug it!",
  };
  await createNewSuggestion(
    strings.televisionAppliance.id,
    suggestion[strings.currentLanguage],
    strings.ecoModeType
  );

  suggestion = {
    it: "Compra una TV dalle giuste dimensioni, senza esagerare: se la distanza è tra 1,6 e 2,4 metri, allora dovete avere una tv da 40 pollici. Infine, tra 1,2 e 1,9 metri va più che bene un televisore di 32 pollici.",
    en: "",
  };
  await createNewSuggestion(
    strings.televisionAppliance.id,
    suggestion[strings.currentLanguage],
    strings.ecoModeType
  );

  suggestion = {
    it: "Quando acquisti una nuova televisione, controlla la classe energetica: dovrebbe essere almeno A++.",
    en: "",
  };
  await createNewSuggestion(
    strings.televisionAppliance.id,
    suggestion[strings.currentLanguage],
    strings.ecoModeType
  );

  suggestion = {
    it: "Spesso il programma 'Cotone' della lavatrice non è davvero necessario. La prossima volta, prova a ragionare su che cosa stai lavando e scegli il programma più adatto e meno energivoro.",
    en: "Was it really necessary to use the 'Cotton' program for the washing machine? Next time, try to think about what you are washing and choose the most suitable and less energy-intensive program.",
  };
  await createNewSuggestion(
    strings.washingmachineAppliance.id,
    suggestion[strings.currentLanguage],
    strings.ecoModeType
  );

  suggestion = {
    it: "La tua lavatrice ha una modalità Eco. Quando possibile, prova a utilizzarla per risparmiare energia.",
    en: "Your washing machine has an Eco mode. When possible, try to use it to save energy.",
  };
  await createNewSuggestion(
    strings.washingmachineAppliance.id,
    suggestion[strings.currentLanguage],
    strings.ecoModeType
  );

  suggestion = {
    it: "Controlla sempre se la lavatrice è a pieno carico, prima di avviarla. Ricorda che avviare una lavatrice non a pieno carico, spreca energia.",
    en: "The washing machine you just run, is full? Remember that starting a washing machine not full, wastes energy.",
  };
  await createNewSuggestion(
    strings.washingmachineAppliance.id,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "Se attivi la lavatrice dal lunedì al venerdì, dalle 7:00 alle 8.00 e dalle 19:00 alle 23, il sabato dalle ore 7:00 alle 23:00 (tranne le festività nazionali) o dalle 0:00 alle 7.00 e dalle 23:00 alle 24, spendi meno.",
    en: "",
  };
  await createNewSuggestion(
    strings.washingmachineAppliance.id,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "I 30° sono la temperatura migliore per lavare buona parte dei tuoi capi.",
    en: "",
  };
  await createNewSuggestion(
    strings.washingmachineAppliance.id,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "Considera la possibilità di stendere, invece che utilizzare l'asciugatrice, se possibile.",
    en: "Have you considered the possibility of drying, instead of using the dryer machine?",
  };
  await createNewSuggestion(
    strings.driermachineAppliance.id,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "Usa l'asciugatrice solo nei mesi umidi e freddi e senza usare programmi alla massima temperatura.",
    en: "",
  };
  await createNewSuggestion(
    strings.driermachineAppliance.id,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "Metti panni nell'asciugatrice già centrifugati, e fai attenzione che sia sempre a pieno carico.",
    en: "",
  };
  await createNewSuggestion(
    strings.driermachineAppliance.id,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "Nell'asciugatrice, scegli il programma stiro che permette di stirare più facilmente: dura e consuma di meno, i capi escono leggermente più umidi.",
    en: "",
  };
  await createNewSuggestion(
    strings.driermachineAppliance.id,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "Non tenere lo sportello del frigo aperto per troppo tempo!",
    en: "Do not keep the fridge door open for too long!",
  };
  await createNewSuggestion(
    strings.fridgeAppliance.id,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "Il tuo frigo è molto vecchio. Considera l'acquisto di un frigo più moderno e efficiente!",
    en: "Have you considered the purchase of a more modern and efficient fridge?",
  };
  await createNewSuggestion(
    strings.fridgeAppliance.id,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "Ovviamente, più il frigo è grande, più consuma. Tienilo in considerazione, quando ne acquisti uno nuovo.",
    en: "",
  };
  await createNewSuggestion(
    strings.fridgeAppliance.id,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "La posizione in cui viene posizionato il frigo è importante per ridurre i consumi: è consigliabile posizionarlo lontano da fonti di calore",
    en: "",
  };
  await createNewSuggestion(
    strings.fridgeAppliance.id,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "Utilizza il microonde alla giusta potenza, senza esagerare.",
    en: "Use the microwave at the right power, without exaggerating!",
  };
  await createNewSuggestion(
    strings.microwaveAppliance.id,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "Quando non lo utilizzi, stacca la spina del microonde.",
    en: "When you do not use it, unplug the microwave!",
  };
  await createNewSuggestion(
    strings.microwaveAppliance.id,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "Considera l'acquisto di un forno più moderno ed efficiente.",
    en: "Have you considered the purchase of a more modern and efficient oven?",
  };
  await createNewSuggestion(
    strings.electricovenAppliance.id,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "Quando possibile, usa il microonde invece che il forno elettrico.",
    en: "When possible, use the microwave instead of the electric oven.",
  };
  await createNewSuggestion(
    strings.electricovenAppliance.id,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "Considera l'acquisto di fornelli ad induzione più moderni ed efficienti, per risparmiare energia.",
    en: "Have you considered the purchase of more modern and efficient induction hobs?",
  };
  await createNewSuggestion(
    strings.inductionhobAppliance.id,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "Fai controllare spesso la caldaia, per evitare delle perdite.",
    en: "Have the boiler checked often, to avoid losses.",
  };
  await createNewSuggestion(
    strings.boilerAppliance.id,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "Installa un termostato per la tua caldaia. Ti aiuterà a risparmiare, ed è obbligatorio per legge.",
    en: "",
  };
  await createNewSuggestion(
    strings.boilerAppliance.id,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "Lasciare la caldaia sempre accesa aiuta a risparmiare, visto che buona parte dei consumi avvengono durante l'accensione.",
    en: "",
  };
  await createNewSuggestion(
    strings.boilerAppliance.id,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "Effettuare una giusta manutenzione sulla caldaia permette di ridurre i consumi.",
    en: "",
  };
  await createNewSuggestion(
    strings.boilerAppliance.id,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "Non fare delle docce troppo lunghe, per non usare troppo la caldaia e sprecare acqua ed energia.",
    en: "Do not take too long showers, to not use too much the boiler and waste water and energy.",
  };
  await createNewSuggestion(
    strings.boilerAppliance.id,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "Stacca le prese degli elettrodomestici che non usi, per non lasciarli in standby e spegnerli totalmente.",
    en: "Unplug the sockets of the appliances you do not use, to not leave them in standby and turn them off completely.",
  };
  await createNewSuggestion(
    null,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "Quando vai in vacanza, ricordati di staccare tutte le spine, la corrente e di chiudere l'acqua.",
    en: "",
  };
  await createNewSuggestion(
    null,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "Continua sempre ad informarti sulle nuove tecnologie in termini di risparmio: internet è un ottimo posto per saperne di più.",
    en: "",
  };
  await createNewSuggestion(
    null,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "Prediligi gli elettrodomestici a basso consumo: sono quelli di classe A+++, A++ o A+.",
    en: "Prefer low-consumption appliances: they are those of classes A+++, A++ or A+.",
  };
  await createNewSuggestion(
    null,
    suggestion[strings.currentLanguage],
    strings.generalType
  );

  suggestion = {
    it: "Quando compri un elettrodomestico, controlla che abbia la modalità ECO o a basso consumo.",
    en: "When you buy an appliance, check that it has the ECO mode or a low consumption mode.",
  };
  await createNewSuggestion(
    null,
    suggestion[strings.currentLanguage],
    strings.ecoModeType
  );

  suggestion = {
    it: "Migliora il certificato energetico della casa, installando una pompa di calore e dei pannelli fotovoltaici, o aggiungendo un cappotto termico",
    en: "Improve the energy certificate of the house, installing a heat pump or photovoltaic panels.",
  };
  await createNewSuggestion(
    null,
    suggestion[strings.currentLanguage],
    strings.ecoModeType
  );

  curiosity = {
    it: "I nuovi schermi televisivi, a cui è stata applicata la retroilluminazione a LED, possono arrivare a consumare il 25% di energia in meno rispetto ai classici televisori LCD e, addirittura, il 40% in meno dei vecchi modelli al plasma",
    en: "The new LED-backlit televisions can consume up to 25% less energy than the classic LCD televisions, and up to 40% less than the old plasma models.",
  };
  await createNewCuriosity(
    strings.televisionAppliance.id,
    curiosity[strings.currentLanguage],
    strings.generalType
  );

  curiosity = {
    it: "In media un moderno televisore consuma in standby, nel corso di un anno, 26 kWh.",
    en: "On average, a modern television consumes in standby, over the course of a year, 26 kWh.",
  };
  await createNewCuriosity(
    strings.televisionAppliance.id,
    curiosity[strings.currentLanguage],
    strings.generalType
  );

  curiosity = {
    it: "Non usare l’asciugatura con aria calda nella lavastoviglie può ridurre di un quarto d’ora i tempi del lavaggio e risparmiare fino al 45% di energia.",
    en: "Not using the hot air drying in the dishwasher can reduce the washing time by a quarter of an hour and save up to 45% of energy.",
  };
  await createNewCuriosity(
    strings.dishwasherAppliance.id,
    curiosity[strings.currentLanguage],
    strings.generalType
  );

  curiosity = {
    it: "Pulire regolarmente (ogni 10 giorni circa) il filtro della lavastoviglie può evitare che si ostruisca e che la macchina consumi più energia.",
    en: "Regularly cleaning (every 10 days or so) the dishwasher filter can prevent it from clogging and the machine from consuming more energy.",
  };
  await createNewCuriosity(
    strings.dishwasherAppliance.id,
    curiosity[strings.currentLanguage],
    strings.generalType
  );

  curiosity = {
    it: "Il 90% del consumo di una lavatrice è dato dal riscaldamento dell’acqua.",
    en: "Did you know that 90% of the consumption of a washing machine is due to the heating of the water.",
  };
  await createNewCuriosity(
    strings.washingmachineAppliance.id,
    curiosity[strings.currentLanguage],
    strings.generalType
  );

  curiosity = {
    it: "Mantenere una lavatrice sana, pulita e ben lubrificata, può ridurre notevolmente il consumo di energia elettrica.",
    en: "Keeping a healthy, clean and well-lubricated washing machine can significantly reduce electricity consumption.",
  };
  await createNewCuriosity(
    strings.washingmachineAppliance.id,
    curiosity[strings.currentLanguage],
    strings.generalType
  );

  curiosity = {
    it: "L’asciugatrice funziona grazie a un sistema di calore che immette aria calda nel cestello, per eliminare tutta l’acqua e l’umidità in eccesso dai capi.",
    en: "The dryer works thanks to a heating system that injects hot air into the basket, to remove all the water and excess humidity from the clothes.",
  };
  await createNewCuriosity(
    strings.driermachineAppliance.id,
    curiosity[strings.currentLanguage],
    strings.generalType
  );

  curiosity = {
    it: "Alcuni modelli di asciugatrici dispongono anche della funzione vapore, con la quale si evitano le pieghe degli indumenti per ritirare panni pronti da usare senza ricorrere al ferro da stiro.",
    en: "Some models of dryers also have the steam function, with which you avoid the creases of the clothes to remove ready-to-wear clothes without using an iron.",
  };
  await createNewCuriosity(
    strings.driermachineAppliance.id,
    curiosity[strings.currentLanguage],
    strings.generalType
  );

  curiosity = {
    it: "L’impatto sui costi della bolletta del frigorifero è considerevole: può arrivare a incidere fino al 20% del totale.",
    en: "The impact on the cost of the electricity bill of the refrigerator is considerable: it can reach up to 20% of the total.",
  };
  await createNewCuriosity(
    strings.fridgeAppliance.id,
    curiosity[strings.currentLanguage],
    strings.generalType
  );

  curiosity = {
    it: "Esistono dei particolari frigoriferi in grado di funzionare senza corrente per la maggior parte dell'anno, grazie a un sistema di raffreddamento naturale.",
    en: "There are some particular refrigerators that can work without power for most of the year, thanks to a natural cooling system.",
  };
  await createNewCuriosity(
    strings.fridgeAppliance.id,
    curiosity[strings.currentLanguage],
    strings.generalType
  );

  curiosity = {
    it: "La temperatura di comfort per il nostro corpo oscilla tra i 22 e i 25 gradi centigradi. Se utilizziamo l’aria condizionata in questo intervallo non c’è possibilità di ammalarsi o di beccarsi un raffreddore.",
    en: "The comfort temperature for our body oscillates between 22 and 25 degrees Celsius. If we use air conditioning in this range there is no possibility of getting sick or catching a cold.",
  };
  await createNewCuriosity(
    strings.airconditionerAppliance.id,
    curiosity[strings.currentLanguage],
    strings.generalType
  );

  curiosity = {
    it: "L'aria condizionata in auto fa consumare circa il 10% di carburante in più rispetto alla guida senza aria condizionata.",
    en: "Air conditioning in the car consumes about 10% more fuel than driving without air conditioning.",
  };
  await createNewCuriosity(
    strings.airconditionerAppliance.id,
    curiosity[strings.currentLanguage],
    strings.generalType
  );

  curiosity = {
    it: "Il microonde è ottimo per riscaldare cibi risparmiando molta energia, rispetto al forno tradizionale",
    en: "The microwave is great for heating food saving a lot of energy, compared to the traditional oven",
  };
  await createNewCuriosity(
    strings.microwaveAppliance.id,
    curiosity[strings.currentLanguage],
    strings.generalType
  );

  curiosity = {
    it: "Il forno a microonde è un elettrodomestico di recente invenzione: la scoperta di questo metodo di cottura risale al 1945 ed è stato commercializzato su larga scala intorno agli anni '70 negli Stati Uniti",
    en: "The microwave oven is a relatively recent invention: the discovery of this cooking method dates back to 1945 and was widely marketed in the United States in the 1970s",
  };
  await createNewCuriosity(
    strings.microwaveAppliance.id,
    curiosity[strings.currentLanguage],
    strings.generalType
  );

  curiosity = {
    it: "Il forno tradizionale può consumare molto di più di un forno a microonde, soprattutto se è vecchio e non è molto efficiente",
    en: "The traditional oven can consume much more than a microwave oven, especially if it is old and not very efficient",
  };
  await createNewCuriosity(
    strings.electricovenAppliance.id,
    curiosity[strings.currentLanguage],
    strings.generalType
  );

  curiosity = {
    it: "Il forno, se posizionato di fianco al frigorifero, consumerà più energia",
    en: "The oven, if placed next to the refrigerator, will consume more energy",
  };
  await createNewCuriosity(
    strings.electricovenAppliance.id,
    curiosity[strings.currentLanguage],
    strings.generalType
  );

  curiosity = {
    it: "I fornelli ad induzione possono garantire una precisione nella cottura elevatissima.",
    en: "Induction cookers can guarantee a very high cooking precision.",
  };
  await createNewCuriosity(
    strings.inductionhobAppliance.id,
    curiosity[strings.currentLanguage],
    strings.generalType
  );

  curiosity = {
    it: "I fornelli ad induzione consumano notevolmente meno rispetto ai fornelli tradizionali.",
    en: "Induction cookers consume considerably less than traditional cookers.",
  };
  await createNewCuriosity(
    strings.inductionhobAppliance.id,
    curiosity[strings.currentLanguage],
    strings.generalType
  );

  curiosity = {
    it: "Una caldaia di ultima generazione permette di risparmiare il 30%",
    en: "A state-of-the-art boiler allows you to save 30%",
  };
  await createNewCuriosity(
    strings.boilerAppliance.id,
    curiosity[strings.currentLanguage],
    strings.generalType
  );

  curiosity = {
    it: "I modelli di caldaia con tecnologia a condensazione costano di più, ma hanno una efficienza molto maggiore e il costo è bilanciato negli anni",
    en: "Boiler models with condensing technology cost more, but have much greater efficiency and the cost is balanced over the years",
  };
  await createNewCuriosity(
    strings.boilerAppliance.id,
    curiosity[strings.currentLanguage],
    strings.generalType
  );

  curiosity = {
    it: "Se sommiamo tutti gli apparecchi di casa che lasciamo, quotidianamente, attaccati alla corrente, i consumi nascosti possono arrivare a pesare anche del 10-15% sulle nostre utenze domestiche.",
    en: "If we sum all the appliances appliances that we leave, daily, connected to the power supply, the hidden costs can weigh even 10-15% on our domestic utilities.",
  };

  await createNewCuriosity(
    null,
    curiosity[strings.currentLanguage],
    strings.generalType
  );

  curiosity = {
    it: "Gli edifici consumano il 40 percento dell’energia mondiale. Metà di questa energia serve a mantenerli caldi o freddi",
    en: "Buildings consume 40 percent of the world's energy. Half of this energy is used to keep them warm or cold",
  };

  await createNewCuriosity(
    null,
    curiosity[strings.currentLanguage],
    strings.generalType
  );

  curiosity = {
    it: "In sei ore, i deserti del mondo ricevono più energia dal Sole di quanta ne consumano gli esseri umani in un anno",
    en: "In six hours, the deserts of the world receive more energy from the Sun than humans consume in a year",
  };

  await createNewCuriosity(
    null,
    curiosity[strings.currentLanguage],
    strings.generalType
  );
}

module.exports = mockDatabaseData;
