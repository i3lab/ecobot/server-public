const fs = require("fs");

function saveSessionLog(newLog) {
  fs.open("logs.txt", "a", 666, function (e, id) {
    fs.write(id, newLog, null, "utf8", function () {
      fs.close(id, function () {});
    });
  });
}

module.exports = saveSessionLog;
