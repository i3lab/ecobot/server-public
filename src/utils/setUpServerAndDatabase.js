const express = require("express");
const cors = require("cors");
const db = require("../models");
const mockDatabaseData = require("./mockDatabaseData");

/**
 * This function is needed to initialise the database and the (express) server when
 * the backend is started (will be executed in index.js).
 */
function setUpServerAndDatabase() {
  const app = express();

  const corsOptions = {
    origin: "http://localhost:3000", // change this port in order to match the frontend port and avoid CORS errors
  };

  app.use(cors(corsOptions));
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));

  // remember to add new routes here to initialise that route API at the app start
  require("../routes/userRoutes")(app);
  require("../routes/areaRoutes")(app);
  require("../routes/consumptionRoutes")(app);
  require("../routes/answerRoutes")(app);
  require("../routes/notificationRoutes")(app);
  require("../routes/suggestionRoutes")(app);
  require("../routes/curiosityRoutes")(app);

  // initialise the database tables
  // set force to true to drop all tables and re-create them every time the app is started. Useful for testing,
  // but must be disabled in production since all data are lost
  db.sequelize
    .sync({ force: true })
    .then(async () => {
      await mockDatabaseData(); // after the tables are created, insert some mock data in the database for testing purposes
    })
    .catch((err) => {
      console.log("Failed to sync db: " + err.message);
    });

  app.get("/", (req, res) => {
    res.json({ message: "Server is running." });
  });

  const PORT = process.env.PORT || 8080;
  app.listen(PORT, () => {}); // listen on port 8080 for incoming API requests
}

module.exports = setUpServerAndDatabase;
