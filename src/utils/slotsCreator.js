const strings = require("../data/strings");

/**
 * Those slots are initialised in the ChatbotManager, through the NLPManager. They
 * are needed by NLPManager to understand when missing information (e.g.: the appliance
 * name when the user is requesting to set a goal) is needed to complete the intent, and to
 * ask them to the user with the 'question' field specified in the addSlot functions below.
 *
 * Please note that a 'slot' is specific for a certain intent, and it's not a global variable.
 * So, if we add appliance slot for setGoal intent, it will be available only for that intent and
 * not for all the intents that contains appliance entity.
 */
function slotsCreator(nlpManager) {
  nlpManager.slotManager.addSlot(
    strings.setGoalIntent,
    strings.applianceEntity,
    true,
    {
      en: strings.whichApplianceSlotQuestion[strings.currentLanguage],
    }
  );
  nlpManager.slotManager.addSlot(
    strings.removeGoalIntent,
    strings.applianceEntity,
    true,
    {
      en: strings.whichApplianceSlotQuestion[strings.currentLanguage],
    }
  );
  nlpManager.slotManager.addSlot(
    strings.editGoalIntent,
    strings.applianceEntity,
    true,
    {
      en: strings.whichApplianceSlotQuestion[strings.currentLanguage],
    }
  );
  nlpManager.slotManager.addSlot(
    strings.decreaseGoalIntent,
    strings.applianceEntity,
    true,
    {
      en: strings.whichApplianceSlotQuestion[strings.currentLanguage],
    }
  );
  nlpManager.slotManager.addSlot(
    strings.increaseGoalIntent,
    strings.applianceEntity,
    true,
    {
      en: strings.whichApplianceSlotQuestion[strings.currentLanguage],
    }
  );
  nlpManager.slotManager.addSlot(
    strings.checkGoalIntent,
    strings.applianceEntity,
    true,
    {
      en: strings.whichApplianceSlotQuestion[strings.currentLanguage],
    }
  );
  nlpManager.slotManager.addSlot(
    strings.costIntent,
    strings.applianceEntity,
    true,
    {
      en: strings.whichApplianceSlotQuestion[strings.currentLanguage],
    }
  );
  nlpManager.slotManager.addSlot(
    strings.checkConsumptionIntent,
    strings.applianceEntity,
    true,
    {
      en: strings.whichApplianceSlotQuestion[strings.currentLanguage],
    }
  );
  nlpManager.slotManager.addSlot(
    strings.suggestionIntent,
    strings.applianceEntity,
    true,
    {
      en: strings.whichApplianceSlotQuestion[strings.currentLanguage],
    }
  );
  nlpManager.slotManager.addSlot(
    strings.curiosityIntent,
    strings.applianceEntity,
    true,
    {
      en: strings.whichApplianceSlotQuestion[strings.currentLanguage],
    }
  );
  nlpManager.slotManager.addSlot(
    strings.compareWithAverageIntent,
    strings.applianceEntity,
    true,
    {
      en: strings.whichApplianceSlotQuestion[strings.currentLanguage],
    }
  );
}

module.exports = slotsCreator;
